﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using Ultrahaptics;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearRegression;


public class RealWorldCalibration : MonoBehaviour {
    GameObject TP1, TP2, TP3, TP4, TP5, UHArray, arrayRef, trackercube;
    public GameObject puckO;
    GameObject[] TP;
    Plane gpPlane;
    public UnityEngine.Vector3 baxterForward, puckPosition;
    UnityEngine.Vector3[] averageIKPosition;
    ROSCommunication ROSCom;
    SteamVR_TrackedObject viveTracker;
    public bool saveCalibration, moveIKPoints, loadCalibration;
    bool startCalibration = false;
    public int calibrationRuns, IKWaitDuration;
    int numTrackedPoints=5;

    void Start ()
    {
        Debug.Log("Real World Calibration Script Starting");
        IdentifyGameObjects(); // list game objects from tags     
        if (loadCalibration) { LoadCalibration(); } // load calibration and move points
    }

    void IdentifyGameObjects()
    {   
        TP1 = GameObject.FindWithTag("TP1"); // Tracked Points
        TP2 = GameObject.FindWithTag("TP2");
        TP3 = GameObject.FindWithTag("TP3");
        TP4 = GameObject.FindWithTag("TP4");
        TP5 = GameObject.FindWithTag("TP5");
        TP5 = GameObject.FindWithTag("TP5");
        GameObject[] TPTemp = {TP1, TP2, TP3, TP4, TP5}; // Create TP array
        TP = TPTemp;
        Debug.Log("About to find tracker: ");
        //puckO = GameObject.FindWithTag("vivetracker");
        
        //viveTracker = puckO.GetComponent<SteamVR_TrackedObject>();
        //Debug.Log(viveTracker.name.ToString());
        GameObject managerObject = GameObject.FindWithTag("Managers");
        ROSCom = managerObject.GetComponent<ROSCommunication>();
    }

    public void StartCalibration() 
    {
        Debug.Log("Starting Calibration Procedure, IKWait duration = " + IKWaitDuration + ", # of Runs = " + calibrationRuns);
        startCalibration = true;
        StartCoroutine(Calibrator()); // Calibration routine
    }

    public GameObject[] GetTPArray()
    {
        GameObject[] tempTP = TP;
        return tempTP;
    }

    IEnumerator Calibrator()
    {
        UnityEngine.Vector3 [] tempViveTracker = new UnityEngine.Vector3 [numTrackedPoints];
        averageIKPosition = new UnityEngine.Vector3[numTrackedPoints];
        UnityEngine.Vector3[] rawTrackedPoints = new UnityEngine.Vector3[numTrackedPoints*calibrationRuns];
        for (int i=0; i<=(calibrationRuns-1); i++) // Run number of times defined by user
        {
            Debug.Log("Calibration Wave " + (i+1) + " out of " + calibrationRuns);
            for (int j = 0; j<tempViveTracker.Length; j++) // for each tracked point
            {
                Debug.Log("last working j value: " + j);
                ROSCom.SendSelectionToROS(j + 1);
                yield return new WaitForSeconds((float)IKWaitDuration);
                //TODO Add technique for sampling puck0 position over time (average over a half second or so)
                tempViveTracker[j] += puckO.transform.position;
                rawTrackedPoints[(5*i)+j] = puckO.transform.position; // plot in (tp1, tp2... tpn, tp1, tp2..tpn) pattern
            }
        }

        for (int i=0; i<tempViveTracker.Length; i++) // for each tracked point
        {
            averageIKPosition[i] = tempViveTracker[i] / calibrationRuns;
            Debug.Log("Puck position for " + (i+1) + " is: " + averageIKPosition[i].ToString("F4"));
        }

        RegressionAnalysis(rawTrackedPoints);
        if (moveIKPoints) { PositionTrackedPoints(averageIKPosition); } // Move Points
        if (saveCalibration) { SaveCalibration(rawTrackedPoints);} // Save Calibration
        Debug.Log("Finished Calibration");
    }

    void PositionTrackedPoints(UnityEngine.Vector3 [] TPLocations)
    {
        Debug.Log("Placing Tracked Points");
        for(int i=0; i<TP.Length; i++)
        {
            TP[i].transform.position = TPLocations[i];
        }
    }

    void SaveCalibration(UnityEngine.Vector3[] rawTrackedPoints) // TODO
    {
        Debug.Log("Starting Calibration Save ");
        string filePath = Application.dataPath + "/Calibration Saves/Saved_Calibration_data.csv";
        StreamWriter writer = new StreamWriter(filePath);
        writer.WriteLine("Name, X , Y, Z"); // Top line in CSV
        for (int i = 0; i<rawTrackedPoints.Length; ++i) //write out tracked points
        {
            writer.WriteLine("TP[" + ((i % 5)+1) + "]" +
                "," + rawTrackedPoints[i].x +
                "," + rawTrackedPoints[i].y +
                "," + rawTrackedPoints[i].z); 
        }
        writer.Flush();
        writer.Close();
        Debug.Log("Saving Calibration Finished");
    }

    void LoadCalibration()
    {
        averageIKPosition = new UnityEngine.Vector3[numTrackedPoints];
        string[] lines = File.ReadAllLines(Application.dataPath + "/Calibration Saves/Saved_Calibration_data.csv");
        UnityEngine.Vector3[] rawTrackedPoints = new UnityEngine.Vector3 [lines.Length-1];
        for (int i = 0; i < lines.Length; i++)
        {
            string[] parts = lines[i].Split(","[0]); // each Part is an entry on the line
            if(i!=0)
            {
                float.TryParse(parts[1], out rawTrackedPoints[i - 1].x); // read raw points strings as floats, store in rawTrackedPoints[]
                float.TryParse(parts[2], out rawTrackedPoints[i - 1].y);
                float.TryParse(parts[3], out rawTrackedPoints[i - 1].z);
            }
        }

        for (int i=0; i<rawTrackedPoints.Length; i++)
        {
            averageIKPosition[i % numTrackedPoints] += rawTrackedPoints[i];
        }
        for (int i=0; i<averageIKPosition.Length; i++)
        {
            averageIKPosition[i] = averageIKPosition[i] / (rawTrackedPoints.Length / numTrackedPoints);
        }

        PositionTrackedPoints(averageIKPosition); // Place Tracked points, basesd on average values
        UnityEngine.Vector3 coefficients = RegressionAnalysis(rawTrackedPoints); // Find Coefficients from Regression Analysis
        CreateBaxterForward(coefficients);

    }

    void CreateBaxterForward(UnityEngine.Vector3 coefficients)
    {
        float slope = coefficients.x;
        float intercept = coefficients.y; // z = slope*x + intercept
        float rSquared = coefficients.z;
        float x1 = 1.0f; // some irrelevant floats for creating 3D plane to find normal
        float x2 = 0.9f;
        float x3 = 1.1f;

        UnityEngine.Vector3 gp1Vector = new UnityEngine.Vector3(x1, 1.5f, (x1 * slope)+intercept); // generated point location on regression line
        UnityEngine.Vector3 gp2Vector = new UnityEngine.Vector3(x2, 0.8f, (x2 * slope) + intercept);
        UnityEngine.Vector3 gp3Vector = new UnityEngine.Vector3(x3, 0.4f, (x3 * slope) + intercept);

        gpPlane = new Plane(gp1Vector, gp2Vector, gp3Vector); // generate plane from generated points
        baxterForward = -gpPlane.normal; // Set plane Normal Vector
    }

    UnityEngine.Vector3 RegressionAnalysis(UnityEngine.Vector3 []dataSet) // TODO
    {
        Debug.Log("Performing Regression Analysis");
        double[] xPositions = new double[dataSet.Length];
        double[] yPositions = new double[dataSet.Length];
        double[] zPositions = new double[dataSet.Length];

        for (int i=0; i<dataSet.Length; i++)
        {
            xPositions[i] = dataSet[i].x;
            yPositions[i] = dataSet[i].y;
            zPositions[i] = dataSet[i].z;
            Debug.Log("xPositions[" + i + "] is: " + xPositions[i].ToString());
            Debug.Log("zPositions[" + i + "] is: " + zPositions[i].ToString());
        }
        MathNet.Numerics.Tuple<double, double> regression = MathNet.Numerics.Fit.Line(xPositions, zPositions);

        double intercept = regression.Item1; // == 10; intercept
        double slope = regression.Item2; // == 0.5; slope

        double rDetSquared =  MathNet.Numerics.GoodnessOfFit.RSquared(xPositions.Select(x => intercept + (slope*x)), zPositions); // Calculate r^2
        Debug.Log("R^2 det value is: " + rDetSquared);
        Debug.Log("Intercept Value is: " + intercept);
        Debug.Log("Slope Value is: " + slope);

        UnityEngine.Vector3 coefficients = new UnityEngine.Vector3((float)slope, (float)intercept, (float)rDetSquared);
        return coefficients;
    }

    void Update()
    {
    }
}
