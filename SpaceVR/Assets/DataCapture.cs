﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using System.IO;

public class DataCapture : MonoBehaviour {
    public GameObject puck;
    Controller leapController;
    public int numOfPositions, interactionCycles, hapticSpatialCycles;
    int count, IKPosition, userSensation;
    float leapConfidence, objectAppearTime, reactionTime;
    public float participantHeight;
    public string participantID;
    public bool rightHanded, saveRecordedData;
    reactionData[] reactionLog;
    List<puckData> puckLog;
    List<handData> handLog;
    List<headData> headLog;
    List<userChoiceData> userChoiceLog;
    List<fingerTipData> fingerTipLog;
    bool recordPuck=false, recordLeap = false, recordHead = false;
    SteamVR_Camera viveHeadset;

    public struct  userChoiceData
    {
        public float timeStamp;
        public int sensation;
        public int userChoice;

        public userChoiceData(float time, int choice, int sense)
        {
            timeStamp = time;
            sensation = sense;
            userChoice = choice;
        }
    }

    public struct reactionData
    {
        public float timeStamp;
        public int position;
        public float reactionTime;

        public reactionData(float time, int selection, float reactTime)
        {
            timeStamp = time;
            position = selection;
            reactionTime = reactTime;
        }
    }

    public struct handData
    {
        public float timeStamp;
        public Hand hand;
        public int userSensation;

        public handData(float time, Hand handLog,  int sensation)
        {
            timeStamp = time;
            hand = handLog;
            userSensation = sensation;
        }
    }

    public struct puckData
    {
        public float timeStamp;
        public Vector3 position;
        public int IKPosition;

        public puckData(float time, Vector3 pos, int select)
        {
            timeStamp = time;
            position = pos;
            IKPosition = select;
        }
    }

    public struct headData
    {
        public float timeStamp;
        public Vector3 position;
        public int IKPosition;

        public headData(float time, Vector3 pos, int select)
        {
            timeStamp = time;
            position = pos;
            IKPosition = select;
        }
    }

    public struct fingerTipData
    {
        public float timeStamp;
        public string posOnSphere;
        public int IKPosition;
        public Leap.Vector fingerTipPos;
        public Vector3 trueHapticLocation;

        public fingerTipData(float time, string location, int IK, Leap.Vector fingerTip, Vector3 hapticLocation)
        {
            timeStamp = time;
            posOnSphere = location;
            IKPosition = IK;
            fingerTipPos = fingerTip;
            trueHapticLocation = hapticLocation;
        }
    }

    void Start ()
    {
        reactionLog = new reactionData[numOfPositions * interactionCycles];
        count = 0;
        puckLog = new List<puckData>();
        handLog = new List<handData>();
        headLog = new List<headData>();
        fingerTipLog = new List<fingerTipData>();
        userChoiceLog = new List<userChoiceData>();
        leapController = new Controller();
        viveHeadset = new SteamVR_Camera();     
    }

    public void ObjectAppears(int selection)
    {
        IKPosition = selection;
        objectAppearTime = Time.time;
        recordPuck = true;
    }

    public void HandInHapticRegion()
    {
        reactionTime = Time.time - objectAppearTime;
        reactionLog[count] = new reactionData(Time.time, IKPosition, reactionTime);
        recordPuck = false;
        count++;
    }

    public void UserChoice(int userChoice, int currentSensation)
    {
        userChoiceData tempUserChoiceData = new userChoiceData(Time.time, userChoice, currentSensation);
        userChoiceLog.Add(tempUserChoiceData);
    }

    public void StartLeapRecording(int sensation)
    {
        userSensation = sensation;
        recordLeap = true;
    }

    public void StopLeapRecording()
    {
        recordLeap = false;
    }

    public void StartRecordingHeadPosition()
    {
        recordHead = true;
    }
    public void StopRecordingHeadPosition()
    {
        recordHead = false;
    }

    public void RecordTipPosition(string posOnSphere, int IKPosition, Vector3 trueHapticLocation)
    {
        Leap.Vector tempFingerTipPositionData;
        try
        {
            tempFingerTipPositionData = leapController.Frame(0).Hands[0].Fingers[1].TipPosition;
        }
        catch(System.Exception ex)
        {

            tempFingerTipPositionData = new Leap.Vector(0f, 0f, 0f);
            Debug.Log("finger tip not found");
        }
        
        fingerTipData tempFingerTipData = new fingerTipData(Time.time, posOnSphere, IKPosition, tempFingerTipPositionData, trueHapticLocation);
        fingerTipLog.Add(tempFingerTipData);
    }

    public void SaveHapticSpatialStudy()
    {
        if (saveRecordedData)
        {
            Debug.Log("Starting Haptic Spatial Study Save ");
            Debug.Log("Creating Participant Data Folder");
            string folderPath = Application.dataPath + "/Haptic Spatial Studies/" + participantID;
            System.IO.Directory.CreateDirectory(folderPath);

            // Reaction Data
            string filePath = Application.dataPath + "/Haptic Spatial Studies/" + participantID + "/fingerTip.csv";
            Debug.Log("Saving Haptic Spatial Data");
            StreamWriter writer = new StreamWriter(filePath);
            writer.WriteLine("Timestamp, Position On Sphere, IK Position, Finger Tip X, Finger Tip Y, Finger Tip Z, Haptics Location X, Haptics Location Y, Haptics Location Z"); // Top line in CSV
            for (int i = 0; i < fingerTipLog.Count; ++i) //write out reaction data
            {
                writer.WriteLine(
                    fingerTipLog[i].timeStamp +
                    "," + fingerTipLog[i].posOnSphere +
                    "," + fingerTipLog[i].IKPosition +
                    "," + fingerTipLog[i].fingerTipPos.x +
                    "," + fingerTipLog[i].fingerTipPos.y +
                    "," + fingerTipLog[i].fingerTipPos.z +
                    "," + fingerTipLog[i].trueHapticLocation.x +
                    "," + fingerTipLog[i].trueHapticLocation.y +
                    "," + fingerTipLog[i].trueHapticLocation.z);
            }
            writer.Flush();
            writer.Close();
            Debug.Log("Saving Haptic Spatial Study Finished");
        }
    }

    public void SaveInteractionStudyToFile()
    {
        if (saveRecordedData)
        {

            Debug.Log("Starting InteractionStudy Save ");
            Debug.Log("Creating Participant Data Folder");
            string folderPath = Application.dataPath + "/Interaction Studies/" + participantID;
            System.IO.Directory.CreateDirectory(folderPath);

            // Reaction Data
            Debug.Log("Saving Reactions Data");
            string filePath = Application.dataPath + "/Interaction Studies/" + participantID + "/reactions.csv";
            StreamWriter writer = new StreamWriter(filePath);
            writer.WriteLine("Timestamp, Position, Reaction Time"); // Top line in CSV
            for (int i = 0; i < reactionLog.Length; ++i) //write out reaction data
            {
                writer.WriteLine(
                    reactionLog[i].timeStamp +
                    "," + reactionLog[i].position +
                    "," + reactionLog[i].reactionTime);
            }
            writer.Flush();
            writer.Close();

            // User Choice Data
            Debug.Log("Saving Reactions Data");
            filePath = Application.dataPath + "/Interaction Studies/" + participantID + "/userChoices.csv";
            writer = new StreamWriter(filePath);
            writer.WriteLine("Timestamp, Sensation, User Choice"); // Top line in CSV
            for (int i = 0; i < userChoiceLog.Count; ++i) //write out reaction data
            {
                writer.WriteLine(
                    userChoiceLog[i].timeStamp +
                    "," + userChoiceLog[i].sensation +
                    "," + userChoiceLog[i].userChoice);
            }
            writer.Flush();
            writer.Close();

            // Wrist data
            Debug.Log("Saving Puck Data");
            filePath = Application.dataPath + "/Interaction Studies/" + participantID + "/wrist.csv";
            writer = new StreamWriter(filePath);
            writer.WriteLine("Timestamp, IK Position, X Position, Y Position, Z Position"); // Top line in CSV
            for (int i = 0; i < puckLog.Count; ++i) //write wrist position data
            {
                writer.WriteLine(
                    puckLog[i].timeStamp +
                    "," + puckLog[i].IKPosition +
                    "," + puckLog[i].position.x +
                    "," + puckLog[i].position.y +
                    "," + puckLog[i].position.z);
            }
            writer.Flush();
            writer.Close();

            // Leap Data
            Debug.Log("Saving Leap Motion Data");
            filePath = Application.dataPath + "/Interaction Studies/" + participantID + "/hand.csv";
            writer = new StreamWriter(filePath);
            writer.WriteLine("Timestamp, Sensation, Confidence, Direction X, Direction Y, Direction Z, Grab Angle, Grab Strength, Palm Normal X, Palm Normal Y, Palm Normal Z, " +
                "Palm Position X. Palm Position Y, Palm Position Z, Pinch Distance, Pinch Strength, Time Visible, Wrist Position X, Wrist Position Y, Wrist Position Z, Thumb Position X, " +
                "Thumb Position Y, Thumb Position Z, Index Position X, Index Position Y, Index Position Z, Middle Position X, Middle Position Y, Middle Position Z, Ring Position X, " +
                "Ring Position Y, Ring Position Z, Pinky Position X, Pinky Position Y, Pinky Position Z, Palm Velocity X, Palm Velocity Y, Palm Velocity Z "); // Top line in CSV

            for (int i = 0; i < handLog.Count; ++i) //write hand data
            {
                writer.WriteLine(
                    handLog[i].timeStamp +
                    "," + handLog[i].userSensation +
                    "," + handLog[i].hand.Confidence +
                    "," + handLog[i].hand.Direction.x +
                    "," + handLog[i].hand.Direction.y +
                    "," + handLog[i].hand.Direction.z +
                    "," + handLog[i].hand.GrabAngle +
                    "," + handLog[i].hand.GrabStrength +
                    "," + handLog[i].hand.PalmNormal.x +
                    "," + handLog[i].hand.PalmNormal.y +
                    "," + handLog[i].hand.PalmNormal.z +
                    "," + handLog[i].hand.PalmPosition.x +
                    "," + handLog[i].hand.PalmPosition.y +
                    "," + handLog[i].hand.PalmPosition.z +
                    "," + handLog[i].hand.PinchDistance +
                    "," + handLog[i].hand.PinchStrength +
                    "," + handLog[i].hand.TimeVisible +
                    "," + handLog[i].hand.WristPosition.x +
                    "," + handLog[i].hand.WristPosition.y +
                    "," + handLog[i].hand.WristPosition.z +
                    "," + handLog[i].hand.Fingers[0].TipPosition.x +
                    "," + handLog[i].hand.Fingers[0].TipPosition.y +
                    "," + handLog[i].hand.Fingers[0].TipPosition.z +
                    "," + handLog[i].hand.Fingers[1].TipPosition.x +
                    "," + handLog[i].hand.Fingers[1].TipPosition.y +
                    "," + handLog[i].hand.Fingers[1].TipPosition.z +
                    "," + handLog[i].hand.Fingers[2].TipPosition.x +
                    "," + handLog[i].hand.Fingers[2].TipPosition.y +
                    "," + handLog[i].hand.Fingers[2].TipPosition.z +
                    "," + handLog[i].hand.Fingers[3].TipPosition.x +
                    "," + handLog[i].hand.Fingers[3].TipPosition.y +
                    "," + handLog[i].hand.Fingers[3].TipPosition.z +
                    "," + handLog[i].hand.Fingers[4].TipPosition.x +
                    "," + handLog[i].hand.Fingers[4].TipPosition.y +
                    "," + handLog[i].hand.Fingers[4].TipPosition.z +
                    "," + handLog[i].hand.PalmVelocity.x +
                    "," + handLog[i].hand.PalmVelocity.y +
                    "," + handLog[i].hand.PalmVelocity.z);
            }
            writer.Flush();
            writer.Close();

            // Head data
            Debug.Log("Saving Head Data");
            filePath = Application.dataPath + "/Interaction Studies/" + participantID + "/head.csv";
            writer = new StreamWriter(filePath);
            writer.WriteLine("Timestamp, IKPosition, X Position, Y Position, Z Position"); // Top line in CSV
            for (int i = 0; i < headLog.Count; ++i) //write head position data
            {
                writer.WriteLine(
                    headLog[i].timeStamp +
                    "," + headLog[i].IKPosition +
                    "," + headLog[i].position.x +
                    "," + headLog[i].position.y +
                    "," + headLog[i].position.z);
            }
            writer.Flush();
            writer.Close();

            Debug.Log("Saving Interaction Study Finished");
        }
    }

	void Update ()
    {
        if (recordPuck)
        {
            puckData puckDataTemp = new puckData(Time.time, puck == null ? new Vector3(0, 0, 0) : puck.transform.position, IKPosition);
            puckLog.Add(puckDataTemp);
        }
        if (recordLeap)
        {
            Hand temphand;
            try
            {
                 temphand = leapController.Frame(0).Hands[0];
            }
            catch(System.Exception ex)
            {
                Debug.Log("hand not in frame, initializing empty hand class for datalog");
                temphand = new Hand();

            }
            handData handDataTemp = new handData(Time.time, temphand, userSensation);
            handLog.Add(handDataTemp);
        }
        if (recordHead)
        {
            Vector3 tempViveHeadset;
            try
            {
                tempViveHeadset = viveHeadset.transform.position;
            }
            catch (System.Exception ex)
            {
                Debug.Log("no headset found for data recording");
                tempViveHeadset = new Vector3(0f,0f,0f);

            }
            headData headDataTemp = new headData(Time.time, tempViveHeadset, IKPosition);
            headLog.Add(headDataTemp);
        }
    }
}
