﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;

public class LeapManager: MonoBehaviour
{
    Controller leapController;
    public bool handInView = false;
    
    void Start()
    {
        leapController = new Controller();
    }

    void Update()
    {
        if (leapController.Frame(0).Hands.Count >= 1)
        {
            handInView = true;
        }
        else
        {
            handInView = false;
        }
      
    }
}
