﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;

public class GetLeapFingers : MonoBehaviour
{
    Hand leftHand, rightHand, hand;
    Vector leftTipPosition, rightTipPosition;
    Finger indexFinger, pinkyFinger;
    Controller leapController;
    List<Hand> hands;
    
    void Start()
    {
        leapController = new Controller();
        Debug.Log(leapController.ToString());
        Debug.Log("Starting Leap Finger Script");
    }

    void Update()
    {
        int handsPresent = leapController.Frame(0).Hands.Count;
        if (handsPresent >= 1)
        {
            Debug.Log("number of hands present are: " + handsPresent.ToString());
        }
        else
        {
            Debug.Log("No hands present");
        }
        hand = leapController.Frame(0).Hands[0];
        
        //print(hand.inval);
        //if (leapController.Frame(0).Hands.)
        //{
        //    print("no hand present!");
        //}
        //else
        //{
        //    indexFinger = hand.Fingers[1];
        //    pinkyFinger = hand.Fingers[4];

        //    // print("palm position is: " + hand.PalmPosition);
        //    Debug.Log("index finger Vector is: " + indexFinger.TipPosition.ToString());
        //    Debug.Log("pinky finger Vector is: " + pinkyFinger.TipPosition.ToString());
        //    Debug.Log("index finger height is: " + indexFinger.TipPosition.y.ToString());
        //}
        // Debug.Log("location of wrist is: " + hands[0].WristPosition.ToString());
        //Debug.Log("Left Tip position is: " + leftTipPosition.ToString());
        //Debug.Log("Right Tip position is: " + rightTipPosition.ToString());
    }
}
