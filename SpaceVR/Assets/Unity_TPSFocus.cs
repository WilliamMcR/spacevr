﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Ultrahaptics;

public class Unity_TPSFocus : MonoBehaviour {

    

    [Range(0.0f, 5.0f)]
    public double radius = 0.02f;

    [Range(0.0f, 1.0f)]
    public float intensity = 1.0f;

    [Range(20.0f, 500.0f)]
    public double freq = 200.0f;

    private uint _emitterSampleRate;

    float time;
   
    public struct modulated_point
    {
        public Ultrahaptics.Vector3 position;

        public float intensity;

        public double circle_radius;

        public double circle_frequency;

        public Ultrahaptics.Vector3 evaluateAt(float t)
        {
            position.x = (float)Math.Cos(2.0f * Math.PI * circle_frequency * t) * (float)circle_radius;
            position.y = (float)Math.Sin(2.0f * Math.PI * circle_frequency * t) * (float)circle_radius;
            return position;
        }
    }


    modulated_point point;

    TimePointStreamingEmitter _emitter;

    uint _current, _timepoint_count;

    Ultrahaptics.Vector3[] _positions;
    float[] _intensities;

    bool emitting;
	// Use this for initialization
	void Start () {
        _emitter = new TimePointStreamingEmitter();

        point = new modulated_point();
        _emitter.setMaximumControlPointCount(1);
        _emitterSampleRate = _emitter.getSampleRate();

        point.position = new Ultrahaptics.Vector3(0.0f, 0.0f, 0.2f);
        point.circle_frequency = freq;
        point.circle_radius = radius;
        point.intensity = intensity;

        _emitter.setEmissionCallback(callback, point);
        // Instruct the device to call our callback and begin emitting
        _emitter.start();
        emitting = true;
    }

   

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (emitting)
                Stop();
            else
                Start();

        }
        print(point.position);
        time = Time.time;
    }

    void callback(TimePointStreamingEmitter emitter, OutputInterval interval, TimePoint deadline, object user_obj)
    {
        var my_modulated_point = (modulated_point)user_obj;
        double phase_interval = 2.0 * Math.PI * my_modulated_point.circle_frequency / emitter.getSampleRate();
        // For each time point in this interval...
        foreach (var tpoint in interval)
        {
            Ultrahaptics.Vector3 position = my_modulated_point.evaluateAt(time);
        
            tpoint.persistentControlPoint(0).setPosition(position);
            tpoint.persistentControlPoint(0).setIntensity(1f);// 0.5f * (1.0f - (float)Math.Cos(my_modulated_point.phase)));
        }
    }

    void Stop()
    {
        // Stop the device
        _emitter.stop();

        // Dispose/destroy the emitter
        _emitter.Dispose();
        _emitter = null;
        emitting = false;
    }
}
