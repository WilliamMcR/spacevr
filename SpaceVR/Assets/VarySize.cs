﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VarySize : MonoBehaviour {
    public bool varySize;

    [Range(0.1f, 1.0f)]
    public float speed;

    float time;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        time = Time.time;
        if (varySize)
        {
            transform.localScale = new Vector3(0.05f, 0.05f, 0.05f) * (Math.Abs((float)Math.Sin(time * Math.PI * speed)) + 1.0f);
        }
	}
}
