﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPositioning : MonoBehaviour {
    GameObject saturnV, star, earth, lander, TP0, TP1, TP2, TP3, TP4, TP5, UHArray, arrayRef, leapRef, sensationController, leapController;
    GameObject[] TP;
    GameObject realWorldCalibration;
    RealWorldCalibration calibrationScript;
    Vector3 baxterForward;
    int lastSelection;

    void Start()
    {
        Debug.Log("Object Positioning Script Starting");
        IdentifyGameObjects();
        RotateTrackedPoints();
        PlaceVirtualObjects();
    }

    void IdentifyGameObjects()
    {
      //  TP0 = GameObject.FindWithTag("TP0");//tracked points
        TP1 = GameObject.FindWithTag("TP1");
        TP2 = GameObject.FindWithTag("TP2");
        TP3 = GameObject.FindWithTag("TP3");
        TP4 = GameObject.FindWithTag("TP4");
        TP5 = GameObject.FindWithTag("TP5");
        GameObject[] TPTemp = {TP1, TP2, TP3, TP4, TP5 }; // Create TP array
        TP = TPTemp;

        saturnV = GameObject.FindWithTag("saturnV");
        earth = GameObject.FindWithTag("earth");
        star = GameObject.FindWithTag("star");
        lander = GameObject.FindWithTag("shuttle");
        UHArray = GameObject.FindWithTag("UHBoard");
        //arrayRef = GameObject.FindWithTag("arrayRef");
        leapRef = GameObject.FindWithTag("leapRef");
        //sensationController = GameObject.FindWithTag("sensationController");
        leapController = GameObject.FindWithTag("leapController");
        realWorldCalibration = GameObject.FindWithTag("realWorldCalibration");
        calibrationScript = realWorldCalibration.GetComponent<RealWorldCalibration>();
        baxterForward = calibrationScript.baxterForward;
    }

    void RotateTrackedPoints()
    {
        for (int i = 0; i < 5; i++)
        {
            Debug.Log(baxterForward.ToString());
            TP[i].transform.forward = baxterForward;
        }
    }

    void PlaceVirtualObjects()
    {
        
        star.transform.position = TP[0].transform.position + (baxterForward * 0.20f);

        saturnV.transform.position = TP[1].transform.position + (baxterForward * 0.20f) + (saturnV.transform.up * 0.1f);
        saturnV.transform.forward = baxterForward;
        saturnV.transform.Rotate(90.0f, 0.0f, 0.0f);

        lander.transform.position = TP[2].transform.position + (baxterForward * 0.20f);
        lander.transform.Rotate(-90.0f, 0.0f, 0.0f);
        lander.transform.forward = baxterForward;

        earth.transform.position = TP[3].transform.position + (baxterForward * 0.20f);
        // TODO - Add other virtual objects
    }

    public void MoveVirtualObjectsInteractionStudy(int IKPosition)
    {
        //star.SetActive(true);
        //earth.SetActive(true);
        //saturnV.SetActive(true);
        //lander.SetActive(true);
        Vector3 virtualObjectsLocation = (TP[IKPosition - 1].transform.position + (baxterForward * 0.2f)) + new Vector3(0f, 0.2f, 0f);
        star.transform.position = virtualObjectsLocation + (TP[IKPosition - 1].transform.right * 0.25f);
        saturnV.transform.position = virtualObjectsLocation + (TP[IKPosition - 1].transform.right * 0.5f);
        lander.transform.position = virtualObjectsLocation + (TP[IKPosition - 1].transform.right * -0.25f);
        earth.transform.position = virtualObjectsLocation + (TP[IKPosition - 1].transform.right * -0.5f);
    }

    public void ShowVirtualObject(int Selection)
    {
        Debug.Log("showing new virtual object #: " + Selection);
        star.SetActive(false);
        earth.SetActive(false);
        saturnV.SetActive(false);
        lander.SetActive(false);
        //TODO add other objects

        switch (Selection)
        {
            case 0:
                // no Sensation chosen
                break;
            case 1:
                star.SetActive(true);
                break;
            case 2:
                saturnV.SetActive(true);               
                break;
            case 3:
                lander.SetActive(true);            
                break;
            case 4:
                earth.SetActive(true);                
                break;
            case 5:
                //do something
                break;
            case 6:
                //do somethin                
                break;
        }
    }

    public void PlaceLocationReferences(int Selection) // take an int (0-5) for array location
    {
        Debug.Log("Placing UH Array and Leap Controller gameobjects at selection #: " + Selection);
        UHArray.transform.position = TP[Selection-1].transform.position + (baxterForward * -0.01f);
        UHArray.transform.rotation = TP[Selection-1].transform.rotation; // Orientation of UHArray
        UHArray.transform.Rotate(-90, 0, 0, Space.Self);
        leapController.transform.position = leapRef.transform.position;
        leapController.transform.rotation = leapRef.transform.rotation;
    }

    // Update is called once per frame
    void Update () {
	}
}
