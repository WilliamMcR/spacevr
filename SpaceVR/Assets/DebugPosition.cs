﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugPosition : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        print("X position is: " + transform.position.x);
        print("Y position is: " + transform.position.y);
        print("Z position is: " + transform.position.z);
    }
}
