﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Leap;

public class TestWriterDelete : MonoBehaviour
{

    Vector3 nullVector;
    Controller controller;
    Hand hand;

    void Start()
    {
        Debug.Log("starting the journey");
        //try
        //{
        //GameObject go = GameObject.Find("Managers");
        //    nullVector = go.transform.position;
        //    Debug.Log("it worked?");
        //}
        //catch(System.NullReferenceException ex)
        //{
        //    Debug.Log("CATCHCATCHCATCH:");
        //    nullVector = new Vector3(0, 0, 0);
        //}

        //if (go)
        //{
        //    nullVector = go.transform.position;
        //    nullVector = new Vector3(1f, 2f, 3f);
        //    Debug.Log("true");
        //}
        //else if(!go)
        //{
        //    Debug.Log("false");
        //}

        controller = new Controller();
        if(controller.Frame(0).Hands[0] == null)
        {
            Debug.Log("found to be null");
        }
        else
        {
            Debug.Log("not found to be null");
        }
        try
        {
            Debug.Log("Made it this far:");
            hand = controller.Frame(0).Hands[0];
            Debug.Log("got passed the hand initialize");
        }
        catch(System.Exception ex)
        {
            Debug.Log("ERORORORORORORO");
            hand = new Hand();
        }
        Debug.Log("got passed the try catch statements");
        //nullVector = hand.PalmPosition;


        string folderPath = Application.dataPath + "/Interaction Studies/Test Folder";
        System.IO.Directory.CreateDirectory(folderPath);

        string filePath = Application.dataPath + "/Interaction Studies/" + "Test Folder" + "/tester.csv";
        StreamWriter writer = new StreamWriter(filePath);
        //writer.WriteLine("X: ," + (go==true? nullVector.x :0) + ", Y: ," + (go == true ? nullVector.y : 0) + ", Z: ," + (go == true ? nullVector.z : 0)); //How to log null references into CSV
        writer.WriteLine("X: ," + hand.PalmPosition.x + ", Y: ," + hand.PalmPosition.y + ", Z: ," + hand.PalmPosition.z);
        writer.Flush();
        writer.Close();
        Debug.Log("test csv written");
    }
}
    
