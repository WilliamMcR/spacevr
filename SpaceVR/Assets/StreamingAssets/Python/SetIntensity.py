from pysensationcore import *

# === SetIntensity Block ===
# A Block which can be used to modify the intensity (strength) of a Sensation
intensityBlock = defineBlock("SetIntensity")
defineInputs(intensityBlock, "intensity", "point")
defineBlockInputDefaultValue(intensityBlock.intensity, (1, 0, 0))

def setIntensity(inputs):
    intensity = inputs[0][0]
    point = inputs[1]

    return point[0], point[1], point[2], intensity


defineOutputs(intensityBlock, "out")
defineBlockOutputBehaviour(intensityBlock.out, setIntensity)

