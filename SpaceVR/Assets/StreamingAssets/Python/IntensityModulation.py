from pysensationcore import *

import math

# === SetIntensity Block ===
# A Block which can be used to modify the intensity (strength) of a Sensation
intensityModulationBlock = defineBlock("IntensityModulation")
defineInputs(intensityModulationBlock, "t", "point","modulationFrequency")
defineBlockInputDefaultValue(intensityModulationBlock.modulationFrequency, (143.0, 0.0, 0.0))

def modulateIntensity(inputs):
    time = inputs[0][0]
    point = inputs[1]
    modulationFrequency = 143
    
    intensity = 0.5 * (1 - math.cos(2 * math.pi * time * modulationFrequency));
    return point[0],point[1],point[2],intensity

defineOutputs(intensityModulationBlock, "out")
defineBlockOutputBehaviour(intensityModulationBlock.out, modulateIntensity)