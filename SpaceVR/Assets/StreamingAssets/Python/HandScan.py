from pysensationcore import *

import Scan

handScanBlock = defineBlock("HandScan")
defineInputs(handScanBlock,
             "duration",
             "barLength",
             "t",
             "arrayPosition",
             "arrayDirection",
             "arrayNormal",
             "middleFingerPosition",
             "palmPosition",
             "palmDirection",
             "palmNormal")
defineBlockInputDefaultValue(handScanBlock.duration, (2.5, 0, 0))
defineBlockInputDefaultValue(handScanBlock.barLength, (0.1, 0, 0))

defineBlockInputDefaultValue(handScanBlock.arrayPosition, (0, 0, 0))
defineBlockInputDefaultValue(handScanBlock.arrayDirection, (0, 0, 1))
defineBlockInputDefaultValue(handScanBlock.arrayNormal, (0, 1, 0))

defineBlockInputDefaultValue(handScanBlock.middleFingerPosition, (0, 0.2, 0.06))
defineBlockInputDefaultValue(handScanBlock.palmPosition, (0, 0.2, -0.06))
defineBlockInputDefaultValue(handScanBlock.palmDirection, (0, 0, 1))
defineBlockInputDefaultValue(handScanBlock.palmNormal, (0, 1, 0))

barDir = createInstance("CrossProduct", "barDir")
connect(handScanBlock.palmNormal, barDir.lhs)
connect(handScanBlock.palmDirection, barDir.rhs)

scan = createInstance("Scan", "scan")
connect(handScanBlock.t, scan.t)
connect(handScanBlock.duration, scan.duration)
connect(handScanBlock.barLength, scan.barLength)
connect(barDir.out, scan.barDirection)
connect(handScanBlock.palmPosition, scan.animationPathStart)
connect(handScanBlock.middleFingerPosition, scan.animationPathEnd)

render = createInstance("RenderPath", "render")
connect(handScanBlock.t, render.t)
connect(scan.out, render.path)
connect(Constant((1.0/0.015, 0, 0)), render.drawFrequency)

toArray = createInstance("VirtualSpaceToEmitterSpace", "toArray")
connect(handScanBlock.arrayPosition, toArray.arrayPosition)
connect(handScanBlock.arrayDirection, toArray.arrayDirection)
connect(handScanBlock.arrayNormal, toArray.arrayNormal)
connect(render.out, toArray.point)

comparator = createInstance("Comparator", "ComparatorInstance")
connect(handScanBlock.t, comparator.a)
connect(handScanBlock.duration, comparator.b)
connect(Constant((0, 0, 0)), comparator.returnValueIfAGreaterThanB)
connect(Constant((1, 0, 0)), comparator.returnValueIfAEqualsB)
connect(Constant((1, 0, 0)), comparator.returnValueIfALessThanB)

# Hand Scan is a finite Sensation, based on duration.
# We terminate the Sensation by setting its intensity to zero.
setIntensityInstance = createInstance("SetIntensity", "SetIntensityInstance")
connect(comparator.out, setIntensityInstance.intensity)
connect(toArray.out, setIntensityInstance.point)

defineOutputs(handScanBlock, "out")
connect(setIntensityInstance.out, handScanBlock.out)
