from pysensationcore import *

line = defineBlock("Line")
defineInputs(line, "endpointA", "endpointB", "t")
defineOutputs(line, "out")

defineBlockInputDefaultValue(line.endpointA, (-0.04, 0.0, 0.2))
defineBlockInputDefaultValue(line.endpointB, ( 0.04, 0.0, 0.2))

linePathInstance = createInstance("LinePath", "LinePathInstance")

connect(line.endpointA, linePathInstance.endpointA)
connect(line.endpointB, linePathInstance.endpointB)

renderPathInstance = createInstance("RenderPath", "RenderPathInstance")

connect(line.t, renderPathInstance.t)
connect(Constant((125, 0, 0)), renderPathInstance.drawFrequency)
connect(linePathInstance.out, renderPathInstance.path)
connect(renderPathInstance.out, line.out)
