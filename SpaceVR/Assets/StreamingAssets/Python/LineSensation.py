from pysensationcore import *

import PathSensation as path

pathInstance = createInstance("LinePath", "LinePathInstance")

lineSensation = path.createSensationFromPath("LineSensation",
                                                {
                                                    ("endPointA", pathInstance.endpointA) : (-0.04, 0.0, 0.0),
                                                    ("endPointB", pathInstance.endpointB) : (0.04, 0.0, 0.0),
                                                },
                                                output = pathInstance.out,
                                                drawFrequency = 125)
