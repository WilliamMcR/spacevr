import pysensationcore as psc
import SetIntensity

def _isInputDefined(block, inputName):
    try:
        getattr(block, inputName)
        return True
    except:

        return False


def _mapInnerBlocksInputsToTopLevelInputs(inputs, pathSensation):
    for innerBlockPort in inputs:
        topLevelPortName = innerBlockPort[0]
        innerBlockPortHandle = innerBlockPort[1]

        if not _isInputDefined(pathSensation, topLevelPortName):
            psc.defineInputs(pathSensation, topLevelPortName)

        psc.defineBlockInputDefaultValue(getattr(pathSensation, topLevelPortName), inputs[innerBlockPort])
        psc.connect(getattr(pathSensation, topLevelPortName), innerBlockPortHandle)


def createSensationFromPath(sensationName, inputs, output, drawFrequency=100, intensity=None, trackPalm=False):
    """
    Defines a block to output a path as provided by the instance.

    It creates additional inputs to the block which relate to a transformation pipeline
    which transforms the path as follows:
    Sensation Space -> Virtual Space -> Object in Virtual Space -> Emitter Space

    :param sensationName: Name of the Block to create
    :param pathBlock: A path generating block
    :param inputs: Dictionary of inputs of the path generating block: {("nameTopLevelInput", handleInnerBlockInput):  defaultValue}
    :param output: Instance output of the path generating block instance
    :param drawFrequency: Frequency in which to draw the path
    :param intensity: Instance output of the block instance that sets the intensity of the sensation
    :param trackPalm: If True, the Sensation will track the plane of the palm. (default=False)
    :return: A block named as specified with the transformation pipeline appended to the path block
    """

    pathSensation = psc.defineBlock(sensationName)

    _mapInnerBlocksInputsToTopLevelInputs(inputs, pathSensation)

    if not _isInputDefined(pathSensation, "t"):
        psc.defineInputs(pathSensation, "t")

    if not _isInputDefined(pathSensation, "offset"):
        psc.defineInputs(pathSensation, "offset")

    if intensity is None:
        if not _isInputDefined(pathSensation, "intensity"):
            psc.defineInputs(pathSensation, "intensity")
            psc.defineBlockInputDefaultValue(pathSensation.intensity, (1, 0, 0))

    if not _isInputDefined(pathSensation, "drawFrequency"):
        psc.defineInputs(pathSensation, "drawFrequency")
        psc.defineBlockInputDefaultValue(pathSensation.drawFrequency, (drawFrequency, 0.0, 0.0))

    if trackPalm:
        psc.defineInputs(pathSensation,
                    "palmPosition",
                    "palmDirection",
                    "palmNormal"
                    )

    psc.defineInputs(pathSensation,
                 "sensationXInVirtualSpace",
                 "sensationYInVirtualSpace",
                 "sensationZInVirtualSpace",
                 "sensationOriginInVirtualSpace",

                 "virtualEmitterXInVirtualSpace",
                 "virtualEmitterYInVirtualSpace",
                 "virtualEmitterZInVirtualSpace",
                 "virtualEmitterOriginInVirtualSpace",

                 "virtualXInEmitterSpace",
                 "virtualYInEmitterSpace",
                 "virtualZInEmitterSpace",
                 "virtualOriginInEmitterSpace",
                 )

    psc.defineBlockInputDefaultValue(pathSensation.offset, (0, 0, 0))

    psc.defineBlockInputDefaultValue(pathSensation.sensationXInVirtualSpace, (1, 0, 0))
    psc.defineBlockInputDefaultValue(pathSensation.sensationYInVirtualSpace, (0, 1, 0))
    psc.defineBlockInputDefaultValue(pathSensation.sensationZInVirtualSpace, (0, 0, 1))
    psc.defineBlockInputDefaultValue(pathSensation.sensationOriginInVirtualSpace, (0, 0, 0))

    psc.defineBlockInputDefaultValue(pathSensation.virtualEmitterXInVirtualSpace, (1, 0, 0))
    psc.defineBlockInputDefaultValue(pathSensation.virtualEmitterYInVirtualSpace, (0, 1, 0))
    psc.defineBlockInputDefaultValue(pathSensation.virtualEmitterZInVirtualSpace, (0, 0, 1))
    psc.defineBlockInputDefaultValue(pathSensation.virtualEmitterOriginInVirtualSpace, (0, 0, 0))

    psc.defineBlockInputDefaultValue(pathSensation.virtualXInEmitterSpace, (1, 0, 0))
    psc.defineBlockInputDefaultValue(pathSensation.virtualYInEmitterSpace, (0, 1, 0))
    psc.defineBlockInputDefaultValue(pathSensation.virtualZInEmitterSpace, (0, 0, 1))
    psc.defineBlockInputDefaultValue(pathSensation.virtualOriginInEmitterSpace, (0, 0, 0))

    psc.defineOutputs(pathSensation,
                  "out"
                  )

    composeSensationToVirtualSpaceTransformInstance = \
        psc.createInstance("ComposeTransform", "ComposeSensationToVirtualSpaceTransformInstance")
    transformPathSensationToVirtualSpaceInstance = \
        psc.createInstance("TransformPath", "TransformPathSensationToVirtualSpaceInstance")

    psc.connect(pathSensation.sensationXInVirtualSpace, composeSensationToVirtualSpaceTransformInstance.x)
    psc.connect(pathSensation.sensationYInVirtualSpace, composeSensationToVirtualSpaceTransformInstance.y)
    psc.connect(pathSensation.sensationZInVirtualSpace, composeSensationToVirtualSpaceTransformInstance.z)
    psc.connect(pathSensation.sensationOriginInVirtualSpace, composeSensationToVirtualSpaceTransformInstance.o)

    psc.connect(composeSensationToVirtualSpaceTransformInstance.out, transformPathSensationToVirtualSpaceInstance.transform)

    psc.connect(output, transformPathSensationToVirtualSpaceInstance.path)

    # Virtual Space

    composeObjectInVirtualSpaceTransformInstance = \
        psc.createInstance("ComposeTransform", "ComposeObjectInVirtualSpaceTransformInstance")
    transformPathToObjectInVirtualSpaceInstance = \
        psc.createInstance("TransformPath", "TransformPathToObjectInVirtualSpaceInstance")

    if not trackPalm:
        psc.connect(psc.Constant((1, 0, 0)), composeObjectInVirtualSpaceTransformInstance.x)
        psc.connect(psc.Constant((0, 1, 0)), composeObjectInVirtualSpaceTransformInstance.y)
        psc.connect(psc.Constant((0, 0, 1)), composeObjectInVirtualSpaceTransformInstance.z)
        psc.connect(pathSensation.offset, composeObjectInVirtualSpaceTransformInstance.o)
    else:
        palmTransverseInstance = psc.createInstance("CrossProduct", "palmTransverse")
        psc.connect(pathSensation.palmNormal, palmTransverseInstance.lhs)
        psc.connect(pathSensation.palmDirection, palmTransverseInstance.rhs)

        psc.connect(palmTransverseInstance.out, composeObjectInVirtualSpaceTransformInstance.x)
        psc.connect(pathSensation.palmNormal, composeObjectInVirtualSpaceTransformInstance.y)
        psc.connect(pathSensation.palmDirection, composeObjectInVirtualSpaceTransformInstance.z)
        psc.connect(pathSensation.palmPosition, composeObjectInVirtualSpaceTransformInstance.o)

    psc.connect(composeObjectInVirtualSpaceTransformInstance.out, transformPathToObjectInVirtualSpaceInstance.transform)

    psc.connect(transformPathSensationToVirtualSpaceInstance.out, transformPathToObjectInVirtualSpaceInstance.path)

    # Transformed to Object in Virtual Space

    composeInverseEmitterTransformInstance = \
        psc.createInstance("ComposeInverseTransform", "ComposeInverseEmitterTransformInstance")
    transformPathVirtualToVirtualEmitter = \
        psc.createInstance("TransformPath", "TransformPathVirtualToVirtualEmitter")

    psc.connect(pathSensation.virtualEmitterXInVirtualSpace, composeInverseEmitterTransformInstance.x)
    psc.connect(pathSensation.virtualEmitterYInVirtualSpace, composeInverseEmitterTransformInstance.y)
    psc.connect(pathSensation.virtualEmitterZInVirtualSpace, composeInverseEmitterTransformInstance.z)
    psc.connect(pathSensation.virtualEmitterOriginInVirtualSpace, composeInverseEmitterTransformInstance.o)

    psc.connect(composeInverseEmitterTransformInstance.out, transformPathVirtualToVirtualEmitter.transform)

    psc.connect(transformPathToObjectInVirtualSpaceInstance.out, transformPathVirtualToVirtualEmitter.path)

    # Move Origin to Emitter in Virtual Space

    composeVirtualToEmitterSpaceTransformInstance = \
        psc.createInstance("ComposeTransform", "ComposeVirtualToEmitterSpaceTransformInstance")
    transformPathVirtualToEmitterSpaceInstance = \
        psc.createInstance("TransformPath", "TransformPathVirtualToEmitterSpaceInstance")

    psc.connect(pathSensation.virtualXInEmitterSpace, composeVirtualToEmitterSpaceTransformInstance.x)
    psc.connect(pathSensation.virtualYInEmitterSpace, composeVirtualToEmitterSpaceTransformInstance.y)
    psc.connect(pathSensation.virtualZInEmitterSpace, composeVirtualToEmitterSpaceTransformInstance.z)
    psc.connect(pathSensation.virtualOriginInEmitterSpace, composeVirtualToEmitterSpaceTransformInstance.o)

    psc.connect(composeVirtualToEmitterSpaceTransformInstance.out, transformPathVirtualToEmitterSpaceInstance.transform)

    psc.connect(transformPathVirtualToVirtualEmitter.out, transformPathVirtualToEmitterSpaceInstance.path)

    # Emitter Space

    renderPathInstance = psc.createInstance("RenderPath", "RenderPathInstance")
    psc.connect(pathSensation.t, renderPathInstance.t)
    psc.connect(pathSensation.drawFrequency, renderPathInstance.drawFrequency)
    psc.connect(transformPathVirtualToEmitterSpaceInstance.out, renderPathInstance.path)

    # Sensation Space

    # Intensity control
    setIntensityInstance = psc.createInstance("SetIntensity", "SetIntensityInstance")
    psc.connect(renderPathInstance.out, setIntensityInstance.point)

    if intensity is not None:
        psc.connect(intensity, setIntensityInstance.intensity)
    else:
        psc.connect(pathSensation.intensity, setIntensityInstance.intensity)

    psc.connect(setIntensityInstance.out, pathSensation.out)

    return pathSensation
