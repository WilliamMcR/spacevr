from pysensationcore import *

import PathSensation as path

pathInstance = createInstance("CirclePath", "CirclePathInstance")

circleSensation = path.createSensationFromPath("CircleSensation",
                                                {
                                                    ("radius", pathInstance.radius) : (0.02, 0, 0),
                                                },
                                                output = pathInstance.out,
                                                drawFrequency = 70)
