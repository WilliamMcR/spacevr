from pysensationcore import *

import PathSensation as path

lissajousPath = createInstance("Lissajous", "LissajousInstance")

lissajousSensation = path.createSensationFromPath("LissajousSensation",
                                                  {
                                                      ("sizeX", lissajousPath.sizeX) : (0.01, 0.0, 0.0),
                                                      ("sizeY", lissajousPath.sizeY) : (0.01, 0.0, 0.0),
                                                      ("paramA", lissajousPath.paramA) : (3.0, 0.0, 0.0),
                                                      ("paramB", lissajousPath.paramB) : (2.0, 0.0, 0.0)
                                                  },
                                                  output = lissajousPath.out,
                                                  drawFrequency = 40)
