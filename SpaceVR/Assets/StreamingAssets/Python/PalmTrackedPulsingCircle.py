from pysensationcore import *

import PathSensation as path
import TriangleWave

pathInstance = createInstance("CirclePath", "CirclePathInstance")
triangleWaveBlockInstance = createInstance("TriangleWave", "triangleWave")
connect(triangleWaveBlockInstance.out, pathInstance.radius)

palmTrackedPulsingCircle = path.createSensationFromPath("PalmTrackedPulsingCircle",
                                                {
                                                    ("t", triangleWaveBlockInstance.t) : (0,0,0),
                                                    ("Start Radius (m)", triangleWaveBlockInstance.minValue) : (0.01, 0, 0),
                                                    ("End Radius (m)", triangleWaveBlockInstance.maxValue) : (0.05, 0, 0),
                                                    ("Pulse Period (s)", triangleWaveBlockInstance.period) : (5.0, 0, 0),
                                                },
                                                output = pathInstance.out,
                                                drawFrequency = 70,
                                                intensity = None,
                                                trackPalm = True)
