﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Ultrahaptics;
using Leap;

public class HapticSphereCreator : MonoBehaviour {
    SensationSource[] sensationArray;
    GameObject sensationController;
    Controller leapController;
    bool boardOn=false, hapticsOn=false;
    float palmHeight;
    float virtualObjectOffset = 0.2f;
    float sphereRadius = 0.05f;
    Hand hand;

    // Use this for initialization
    void Start ()
    {
        sensationController = GameObject.FindWithTag("sensationController");
        sensationArray = sensationController.GetComponents<SensationSource>();
        leapController = new Controller();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            boardOn = true;
        }

        hand = leapController.Frame(0).Hands[0];
        palmHeight = (hand.PalmPosition.y / 1000);
        //Debug.Log(hand.PalmPosition.ToString());
        if(palmHeight >= (virtualObjectOffset-sphereRadius) && palmHeight <= (virtualObjectOffset + sphereRadius))
        {
            Debug.Log("hand in range");
            Debug.Log("hand height is: " + palmHeight);
            hapticsOn = true;
        
        }
        else
        {
            Debug.Log("hand not in haptic sphere");
            Debug.Log("hand height is: " + palmHeight);
            hapticsOn = false;
        }

        if(boardOn == true && hapticsOn==true)
        {
            sensationArray[0].Inputs["offset"] = new UnityEngine.Vector3(0, palmHeight, 0);
            sensationArray[0].Inputs["radius"] = new UnityEngine.Vector3(sphereRadius * (float)System.Math.Sin(((palmHeight + sphereRadius - virtualObjectOffset) / (2 * sphereRadius)) * System.Math.PI), 0f, 0f);
            sensationArray[0].Running = true;
        }
        
    }
}
