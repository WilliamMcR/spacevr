﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ultrahaptics;
using Leap;

public class SensationController : MonoBehaviour {

    SensationSource[] sensationArray;
    GameObject sensationController, star, saturnV, lander;
    Controller leapController;
    Hand hand;
    int ActiveSensation = 0;
    float virtualObjectsOffset = 0.2f, palmHeight, lineSensationRange = 0.1f, time, spinSpeed;


    void Start () {
        Debug.Log("Sensation Controller Script Starting");
        sensationController = GameObject.FindWithTag("sensationController");
        sensationArray = sensationController.GetComponents<SensationSource>();
        Debug.Log("number of sensations found from RealWorldCalibration script is: " + sensationArray.Length);
        leapController = new Controller();

        star = GameObject.FindWithTag("star");
        saturnV = GameObject.FindWithTag("saturnV");
        lander = GameObject.FindWithTag("shuttle");

        spinSpeed = saturnV.GetComponent<Rotate>().speed;
        //leapController.ClearPolicy(Controller.PolicyFlag.POLICY_OPTIMIZE_HMD);
    }

    public void TurnOffSensations()
    {
        ActiveSensation = 0;
        for(int i = 0; i <= sensationArray.Length; i++)
        {
            sensationArray[i].Running = false;
        }
    }

    public void TurnOnSensation(int selection)
    {
        ActiveSensation = selection;
    }

    private void OnApplicationQuit()
    {
        TurnOffSensations();
    }

    // Update is called once per frame
    void Update()
    {
        hand = leapController.Frame(0).Hands[0];
        palmHeight = (hand.PalmPosition.y / 1000);
        time = Time.time;
        //Debug.Log("Palmheight: " + palmHeight);
        if (true) // TODO - check if hand position is within trigger box.
        {
            Debug.Log("Seensation " + ActiveSensation + " Chosen");
                switch (ActiveSensation)
            {
                case 0:
                    // no Sensation chosen
                    break;
                case 1:
                    float sphereRadius = star.transform.localScale.x / 2f;
                    if (palmHeight >= (virtualObjectsOffset - sphereRadius) && palmHeight <= (virtualObjectsOffset + sphereRadius))
                    {
                        sensationArray[0].Running = true;
                        sensationArray[0].Inputs["offset"] = new UnityEngine.Vector3(0, palmHeight, 0);
                        sensationArray[0].Inputs["radius"] = new UnityEngine.Vector3(sphereRadius * 
                            (float)System.Math.Sin(((palmHeight + sphereRadius - virtualObjectsOffset) 
                            / (2 * sphereRadius)) * System.Math.PI), 0f, 0f);
                        //Debug.Log("Sensation 1 being created");
                    }
                    else
                    {
                        sensationArray[0].Running = false;
                        //Debug.Log("Hand out of Sphere range");
                    }
                    break;
                case 2:
                    sensationArray[1].Inputs["endPointA"] = new UnityEngine.Vector3(0.1f * (float)Math.Sin(Math.PI * time * spinSpeed),
                            0.1f * (float)Math.Cos(Math.PI * time * spinSpeed), 0.0f);
                    sensationArray[1].Inputs["endPointB"] = new UnityEngine.Vector3(0.1f * (float)Math.Sin(Math.PI * time * spinSpeed + Math.PI),
                        0.1f * (float)Math.Cos(Math.PI * time * spinSpeed + Math.PI), 0.0f);
                    if (palmHeight >=(virtualObjectsOffset - lineSensationRange) && palmHeight <= (virtualObjectsOffset + lineSensationRange))
                    {
                    sensationArray[1].Running = true;
                        
                        //saturnV.GetComponent<Rotate>().rotating = true;
                    Debug.Log("Endpoint A = " + sensationArray[1].Inputs["endPointA"].x + ", " + sensationArray[1].Inputs["endPointA"].y);
                    }
                    else
                    {
                        sensationArray[1].Running = false;
                        //Debug.Log("Sensation 2 error");
                    }             
                    break;
                case 3:
                    float landerWidth = lander.transform.localScale.x * 2.0f; //scaled because lander model scale is f***ed
                    if (palmHeight >= (virtualObjectsOffset)
                        && palmHeight <= (virtualObjectsOffset + lander.transform.localScale.z * 5.0f)) //same for these
                    {
                        sensationArray[0].Running = true;
                        sensationArray[0].Inputs["offset"] = new UnityEngine.Vector3(0, palmHeight, 0);
                        sensationArray[0].Inputs["radius"] = new UnityEngine.Vector3(landerWidth + (virtualObjectsOffset-palmHeight + 0.1f) * 0.25f, 0, 0);
                    }
                    else
                        sensationArray[0].Running = false;
                    Debug.Log("Lander radius = " + sensationArray[0].Inputs["radius"].x);
                    break;
                case 4:
                    //sensationArray[0].Running = true;
                    //sensationArray[0].Inputs["offset"] = new UnityEngine.Vector3(0, palmHeight, 0);
                    //sensationArray[0].Inputs["radius"] = new UnityEngine.Vector3(6.0f * Math.Abs((float)Math.Sin(2.0f * Math.PI * time * 20.0f)), 0.0f, 0.0f);
                    break;
                case 5:
                    //do somethin                
                    break;
                case 6:
                    //do somethin                
                    break;
                default:
                    //TODO - something here?
                    break;
            }
        }
    }
}
