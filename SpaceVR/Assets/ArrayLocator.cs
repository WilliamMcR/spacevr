using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Helper class to enable locating arrays so their positions correspond
/// between the virtual world and real world
///
/// Usage instructions:
/// - Place the root GameObjects of all the arrays you want to locate in the Locatable Objects
///   list in the Unity inspector (use objects in the hierarchy, not prefabs)
/// - When the game is running, pull the trigger to locate the arrays in order
/// - To relocate an array, move the controller close to it in the game world and
///   press the pad, this will make it the next object to be located
/// </summary>
public class ArrayLocator : MonoBehaviour
{
	[SerializeField]
	[Tooltip("If true, ArrayLocator will ensure that every object located is not rotated about the X or Z axes")]
	private bool _alwaysParallelToFloor;

	// Expose this field to the Unity inspector so it can be set to the GameObjects to be located
	[SerializeField]
	private List<GameObject> _locatableObjects;

	// Maximum distance in metres between controller and array to be re-located when the pad is clicked
	private float _relocateDistanceThreshold = 0.1f;

	// A collection of all the objects that still require locating
	private Stack<GameObject> _objectsRequiringLocating;

	// Use this for initialization
	void Start()
	{
		// Initialise the stack by reversing the provided list of locatable objects (as Stack is LIFO)
		_objectsRequiringLocating = new Stack<GameObject>(_locatableObjects.AsEnumerable().Reverse());

		var steamVrManager = FindObjectOfType<SteamVR_ControllerManager>().gameObject;
		// Get all controllers regardless of whether they are active in hierarchy
		var controllers = steamVrManager.GetComponentsInChildren<SteamVR_TrackedController>(true);

		if(controllers.Length != 2)
		{
			Debug.LogWarning("Couldn't find both controllers, could only find " + controllers.Length);
		}

		// Hook up events for both controllers
		foreach(var controller in controllers)
		{
			controller.TriggerClicked += OnClickTrigger;
			controller.PadClicked += OnPadClicked;
		}
	}

	void OnClickTrigger(object sender, ClickedEventArgs e)
	{
		// Position the object at the top of the stack when trigger is pulled
		DoLocating((SteamVR_TrackedController)sender);
	}

	void OnPadClicked(object sender, ClickedEventArgs e)
	{
		// Set the closest object to the controller as the next to be located when pad is clicked
		RelocateClosestObject((SteamVR_TrackedController)sender);
	}

	/// <summary>
	/// Finds the closest locatable object to the given controller (up to the relocate
	/// distance threshold) and moves it to the top of the stack of objects requiring locating.
	/// </summary>
	/// <param name="controller">The controller whose position to use.</param>
	void RelocateClosestObject(SteamVR_TrackedController controller)
	{
		// Get the position of the tip of the controller
		var controllerPos = GetTipPosition(controller);

		// Iterate through the locatable objects and find the closest
		GameObject closestObject = null;
		float lowestDistance = float.MaxValue;
		foreach(var locatableObject in _locatableObjects)
		{
			var distance = (locatableObject.transform.position - controllerPos).magnitude;
			if(distance < _relocateDistanceThreshold && distance < lowestDistance)
			{
				lowestDistance = distance;
				closestObject = locatableObject;
			}
		}

		if(closestObject == null)
		{
			// There are no objects below the relocate distance threshold
			return;
		}

		if(_objectsRequiringLocating.Contains(closestObject))
		{
			// This object is already in the stack, remove it so it can be pushed to the top
			var list = _objectsRequiringLocating.ToList();
			list.Remove(closestObject);
			_objectsRequiringLocating = new Stack<GameObject>(list);
		}

		// Push the closest object to the top of the stack of objects requiring locating
		_objectsRequiringLocating.Push(closestObject);
	}

	/// <summary>
	/// Position the object at the top of the stack at the tip of the given controller,
	/// and the same orientation of the flat surface at the end of the given controller.
	/// </summary>
	/// <param name="controller">The controller whose position and orientation to use.</param>
	void DoLocating(SteamVR_TrackedController controller)
	{
		if(_objectsRequiringLocating.Count == 0)
		{
			// Check that there is actually an object to locate
			Debug.Log("All objects located. To relocate an object, press the pad close to it.");
			return;
		}

		// Get the object at the top of the stack
		var currentObject = _objectsRequiringLocating.Pop();

		// Get the position and orientation of the tip of the given controller
		var controllerPos = GetTipPosition(controller);
		var controllerRot = GetTipRotation(controller);

		// Set the position of the object to that of the controller tip
		currentObject.transform.position = controllerPos;

		// Get the current orientation of the object
		var currentEulerAngles = currentObject.transform.eulerAngles;

		// Set the new X and Z euler angles if necessary
		var eulerX = currentEulerAngles.x;
		var eulerZ = currentEulerAngles.z;
		if(!_alwaysParallelToFloor)
		{
			eulerX = controllerRot.eulerAngles.x;
			// Array 'up' is opposite to controller 'up', so add 180 degrees
			eulerZ = controllerRot.eulerAngles.z + 180f;
		}

		// Set the new orientation of the object
		currentObject.transform.rotation = Quaternion.Euler(eulerX, controllerRot.eulerAngles.y, eulerZ);
	}

	/// <summary>
	/// Gets a quaternion that represents the orientation of the tip of the given controller.
	/// </summary>
	/// <param name="controller">The controller to use.</param>
	/// <returns>The orientation of the tip of the given controller.</returns>
	Quaternion GetTipRotation(SteamVR_TrackedController controller)
	{
		var controllerRotation = controller.gameObject.transform.rotation;
		// The angle between the forward vector of a Vive controller and the flat side of it is 60 degrees
		var rotationOffset = Quaternion.AngleAxis(60, Vector3.right);
		return controllerRotation * rotationOffset;
	}

	/// <summary>
	/// Gets the position of the tip of the given controller (centre of the ring at the end).
	/// </summary>
	/// <param name="controller">The controller to use.</param>
	/// <returns>The position of the tip of the given controller.</returns>
	Vector3 GetTipPosition(SteamVR_TrackedController controller)
	{
		// This is the local offset between the origin of a Vive controller and the top-centre of the ring at the end
		var positionOffset = new Vector3(0f, -0.0242f, 0.0282f);
		var worldOffset = controller.gameObject.transform.TransformVector(positionOffset);
		return controller.gameObject.transform.position + worldOffset;
	}
}
