﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RosSharp.RosBridgeClient;

public class ROSCommunication : MonoBehaviour {
    static public RosSocket rosSocket;
    private int publication_id;
    StandardString message = new StandardString();

    // Use this for initialization
    void Start () {
        Debug.Log("ROS Communication Script Starting");
        rosSocket = new RosSocket("ws://192.168.0.120:9090");
        publication_id = rosSocket.Advertise("/virtual_object", "std_msgs/String");

        // Publication:
        message.data = "First Message";
        rosSocket.Publish(publication_id, message);
    }

    public void SendSelectionToROS(int selection)
    {
        Debug.Log("Sending Selection # " + selection + " to ROS");
        switch (selection)
        {
            case 0:
                // no Sensation chosen
                break;
            case 1:
                message.data = "1";
                break;
            case 2:
                message.data = "2";
                break;
            case 3:
                message.data = "3";
                break;
            case 4:
                message.data = "4";
                break;
            case 5:
                message.data = "5";
                break;
            case 6:
                message.data = "6";
                break;
        }
        rosSocket.Publish(publication_id, message);
    }


    // Update is called once per frame
    void Update ()
    {
     
    }

    void OnApplicationQuit()
    {
        rosSocket.Close();
    }
}
