﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EventManager : MonoBehaviour
{

    ObjectPositioning objectPositioning;
    ROSCommunication ROSCom;
    SensationController SenseControl;
    RealWorldCalibration worldCalibrator;
    LeapManager leapManager;
    DataCapture dataCapture;
    public string mode;
    public bool debugBaxterPositions;
    public int hapticDuration, baxterIKDuration, stepBackDuration, debugSelection;
    int selection, userChoice, numPositions, interactionCycles, hapticSpatialCycles;
    bool selectionMade=false, safeToMove = false, recordIndexTip;
    float time;

    // Use this for initialization
    void Start()
    {
        Debug.Log("Event Manager Script Starting");
        FindScripts();
        mode = "play";
        numPositions = dataCapture.numOfPositions;
        interactionCycles = dataCapture.interactionCycles;
        hapticSpatialCycles = dataCapture.hapticSpatialCycles;

        objectPositioning.PlaceLocationReferences(debugSelection);
        objectPositioning.ShowVirtualObject(debugSelection);
        SenseControl.TurnOnSensation(debugSelection);
    }

    void FindScripts()
    {
        objectPositioning = gameObject.GetComponent<ObjectPositioning>();
        ROSCom = gameObject.GetComponent<ROSCommunication>();
        SenseControl = gameObject.GetComponent<SensationController>();
        GameObject realWorldCalibration = GameObject.FindWithTag("realWorldCalibration");
        worldCalibrator = realWorldCalibration.GetComponent<RealWorldCalibration>();
        leapManager = gameObject.GetComponent<LeapManager>();
        dataCapture = gameObject.GetComponent<DataCapture>();
    }

    public int GetSelection()
    {
        return selection;
    }

    void ModeSelection()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            Debug.Log("Request for Calibration Mode");
            mode = "calibration";
            worldCalibrator.StartCalibration();
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            Debug.Log("Interaction Study Mode Started");
            mode = "interactionstudy";
            StartCoroutine(InteractionStudy()); // Interaction coroutine
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            Debug.Log("Haptic Spatial Study Mode Started");
            mode = "erpstudy";
            StartCoroutine(HapticSpatialStudy());
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("Play Mode Started");
            mode = "play";
        }
    }

    void DebugOverrideROSPosition()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Debug.Log("Position 1 sent");
            ROSCom.SendSelectionToROS(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Debug.Log("Position 2 sent");
            ROSCom.SendSelectionToROS(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Debug.Log("Position 3 sent");
            ROSCom.SendSelectionToROS(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Debug.Log("Position 4 sent");
            ROSCom.SendSelectionToROS(4);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            Debug.Log("Position 5 sent");
            ROSCom.SendSelectionToROS(5);
        }
    }

    void UserVirtualObjectChoice()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Debug.Log("Object 1 chosen");
            selectionMade = true;
            userChoice = 1;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Debug.Log("Object 2 chosen");
            selectionMade = true;
            userChoice = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Debug.Log("Object 3 chosen");
            selectionMade = true;
            userChoice = 3;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Debug.Log("Object 4 chosen");
            selectionMade = true;
            userChoice = 4;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            Debug.Log("Object 5 chosen");
            selectionMade = true;
            userChoice = 5;
        }
    }

    IEnumerator InteractionStudy() // TODO, check that array and leap REFs are being placed.
    {
        Debug.Log("Starting Interaction Study");
        objectPositioning.ShowVirtualObject(0); // Hide all objects
        // TODO Send Instructions to User of the Task
        yield return new WaitForSeconds(1f); // wait while user reads instruct
        int IKposition, choiceNumber=1;
        for (int j = 0; j < interactionCycles; j++)
        {
            IKposition = 1;
            System.Random r = new System.Random();

            foreach (int i in System.Linq.Enumerable.Range(1, numPositions).OrderBy(x => r.Next()))
            {
                Debug.Log("Moving to position " + IKposition);
                ROSCom.SendSelectionToROS(IKposition);
                yield return new WaitForSeconds((float)baxterIKDuration); // wait for baxter
                SenseControl.TurnOnSensation(i + 1); 
                objectPositioning.ShowVirtualObject(IKposition); // TODO change object positioning to show sensation location with all objects above IK position, also have numbers against objects
                dataCapture.ObjectAppears(IKposition);
                dataCapture.StartRecordingHeadPosition();
                yield return new WaitUntil(() => leapManager.handInView == true); // TODO, change to wait for hand to touch virtual object. 
                dataCapture.HandInHapticRegion();
                Debug.Log("hand Detected!");
                dataCapture.StartLeapRecording(i+1);
                yield return new WaitForSeconds((float)hapticDuration); // user interacting with object
                dataCapture.StopLeapRecording();
                dataCapture.StopRecordingHeadPosition();
                Debug.Log("Awaiting input of user Choice # " + choiceNumber);
                yield return new WaitUntil(() => selectionMade == true);
                selectionMade = false;
                dataCapture.UserChoice(userChoice, i + 1);
                objectPositioning.ShowVirtualObject(0); // object disappear
                //TODO Tell user to step back
                Debug.Log("Waiting for space bar press to move to next position (user step back)");
                yield return new WaitUntil(() => safeToMove == true);
                safeToMove = false;
                IKposition++;
                choiceNumber++;
            }

            yield return new WaitForSeconds(1f); // wait while user reads instruct  
        }
        SenseControl.TurnOffSensations();
        Debug.Log("Interaction Study complete");
        dataCapture.SaveInteractionStudyToFile();
        Debug.Log("Interaction Study Saved");
    }

    IEnumerator HapticSpatialStudy()
    {
        int choiceNumber =1;
        Debug.Log("Starting Haptic Spatial Study");
        //TODO Instructions to User
        yield return new WaitForSeconds(1f); // wait while user reads instructions
        System.Random radiusRandomer = new System.Random();
        float[] radiusSize = { 0.02f, 0.03f, 0.04f, 0.05f, 0.06f};
        radiusSize = radiusSize.OrderBy(x => radiusRandomer.Next()).ToArray();

        
        for (int j = 0; j < hapticSpatialCycles; j++)
        {
            System.Random r = new System.Random();
            foreach (int i in System.Linq.Enumerable.Range(1, numPositions).OrderBy(x => r.Next()))
            {
                GameObject[] TP =  worldCalibrator.GetTPArray();
                Vector3 hapticCenterPoint = TP[i-1].transform.position + (TP[i-1].transform.forward * 0.20f);

                float sphereRadius = radiusSize[i-1];
                ROSCom.SendSelectionToROS(i);
                objectPositioning.MoveVirtualObjectsInteractionStudy(i); // Place virtual objects at IK position

                //SenseControl.TurnOnSensation(); /.SEND fixed Sphere sensation, location is IKposition
                // TODO - Show axis with random offset 20cm away from Tracked point

                //tell user to feel around for the sphere and set their index finger tip on the top
                Debug.Log("choice # :" + choiceNumber + ", Waiting for user to indicated when they are satisfied they have located the top");
                yield return new WaitUntil(() => recordIndexTip == true);
                recordIndexTip = false;
                dataCapture.RecordTipPosition("top", i, ((hapticCenterPoint + new Vector3(0,sphereRadius,0))-(TP[i-1].transform.position+new Vector3(0, 0.097f,0)))); // need to edit so vector is using LEAp reference frame

                //tell user to feel around for the sphere and set their index finger tip on the bottom
                Debug.Log("choice # :" + choiceNumber + ", Waiting for user to indicated when they are satisfied they have located the bottom");
                yield return new WaitUntil(() => recordIndexTip == true);
                recordIndexTip = false;
                dataCapture.RecordTipPosition("bottom", i, (hapticCenterPoint + new Vector3(0, -sphereRadius, 0)) - (TP[i - 1].transform.position + new Vector3(0, 0.097f, 0)));

                //tell user to feel around for the sphere and set their index finger tip on the right
                Debug.Log("choice # :" + choiceNumber + ", Waiting for user to indicated when they are satisfied they have located the right");
                yield return new WaitUntil(() => recordIndexTip == true);
                recordIndexTip = false;
                dataCapture.RecordTipPosition("right", i, (hapticCenterPoint + (TP[i-1].transform.right * -sphereRadius)) - (TP[i - 1].transform.position + new Vector3(0, 0.097f, 0)));

                //tell user to feel around for the sphere and set their index finger tip on the left
                Debug.Log("choice # :" + choiceNumber + ", Waiting for user to indicated when they are satisfied they have located the left");
                yield return new WaitUntil(() => recordIndexTip == true);
                recordIndexTip = false;
                dataCapture.RecordTipPosition("left", i, (hapticCenterPoint + (TP[i - 1].transform.right * sphereRadius)) - (TP[i - 1].transform.position + new Vector3(0, 0.097f, 0)));

                //tell user to feel around for the sphere and set their index finger tip on the front
                Debug.Log("choice # :" + choiceNumber + ", Waiting for user to indicated when they are satisfied they have located the front");
                yield return new WaitUntil(() => recordIndexTip == true);
                recordIndexTip = false;
                dataCapture.RecordTipPosition("front", i, (hapticCenterPoint + (TP[i - 1].transform.forward * sphereRadius)) - (TP[i - 1].transform.position + new Vector3(0, 0.097f, 0)));

                //tell user to feel around for the sphere and set their index finger tip on the back
                Debug.Log("choice # :" + choiceNumber + ", Waiting for user to indicated when they are satisfied they have located the back");
                yield return new WaitUntil(() => recordIndexTip == true);
                recordIndexTip = false;
                dataCapture.RecordTipPosition("back", i, (hapticCenterPoint + (TP[i - 1].transform.forward * -sphereRadius)) - (TP[i - 1].transform.position + new Vector3(0, 0.097f, 0)));

                //tell user to feel around for the sphere and set their index finger tip on the center
                Debug.Log("choice # :" + choiceNumber + ", Waiting for user to indicated when they are satisfied they have located the center");
                yield return new WaitUntil(() => recordIndexTip == true);
                recordIndexTip = false;
                dataCapture.RecordTipPosition("center", i, (hapticCenterPoint - (TP[i - 1].transform.position + new Vector3(0, 0.097f, 0))));

                Debug.Log("Finished Position: " + i + ", Waiting for space bar press to move to next position (user step back)");
                yield return new WaitUntil(() => safeToMove == true);
                safeToMove = false;

                choiceNumber++;
            }
        }

        dataCapture.SaveHapticSpatialStudy();
    }


    // Update is called once per frame
    void Update()
    {
        ModeSelection();
        if (debugBaxterPositions) { DebugOverrideROSPosition(); }
        UserVirtualObjectChoice();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Space bar pressed, moving Baxter arm");
            safeToMove = true;
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            Debug.Log("Char T pressed, finger tip position recorded");
            recordIndexTip = true;
        }

        if (mode == "play")
        {
            //TODO GetKEyDown to turn haptics on / off
        }
        else if (mode == "calibration")
        {

        }
        else if (mode == "erpstudy")
        {

        }
        else if (mode == "interactionstudy")
        {

        }
        else if (mode == "shapedefiningstudy")
        {

        }
    }
}