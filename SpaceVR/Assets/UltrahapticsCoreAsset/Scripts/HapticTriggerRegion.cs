﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Ultrahaptics
{
    public class HapticTriggerRegion : MonoBehaviour
    {
        public class EditorHelpBoxAttribute : PropertyAttribute { }

        [EditorHelpBox, SerializeField] private bool _dummyField;
        [SerializeField] protected UnityEvent _onEnter = new UnityEvent();
        [SerializeField] protected UnityEvent _onLeave = new UnityEvent();
        private readonly HashSet<Collider> _entered = new HashSet<Collider>();

        public event UnityAction OnEnter
        {
            add { _onEnter.AddListener(value); }
            remove { _onEnter.RemoveListener(value); }
        }

        public event UnityAction OnLeave
        {
            add { _onLeave.AddListener(value); }
            remove { _onLeave.RemoveListener(value); }
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            if (_entered.Count == 0)
            {
                _onEnter.Invoke();
            }
            _entered.Add(other);
        }

        protected virtual void OnTriggerExit(Collider other)
        {
            if (!_entered.Remove(other))
            {
                Debug.LogWarningFormat("{0} left {1} without first entering it");
            } else if (_entered.Count == 0)
            {
                _onLeave.Invoke();
            }
        }

        private readonly System.Predicate<Collider> IsNull = c => c == null || !c.enabled || !c.gameObject.activeInHierarchy;

        protected virtual void Update()
        {
            if (_entered.Count > 0)
            {
                if (_entered.Count == _entered.RemoveWhere(IsNull))
                {
                    _onLeave.Invoke();
                }
            }
        }
    }
}
