using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Ultrahaptics
{
    [Serializable]
    public class SensationBlockInput
    {
        public string name;
        public UnityEngine.Vector3 value;
    }
}
