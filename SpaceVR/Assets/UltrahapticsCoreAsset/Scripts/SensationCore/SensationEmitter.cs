﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ultrahaptics
{
[DisallowMultipleComponent]
public class SensationEmitter : MonoBehaviour {

    [Header("Visualization Settings")]
    [Tooltip("The transform origin for control point visualization (only required if using an Array Data Source)")]
    public Transform _arrayTransform;
    [Tooltip("The size of the visualized control points")]
    public float _sphereRadius = 0.005f;
    [Tooltip("The color of the visualized control points")]
    public Color _sphereColor = new Color(100.0f/255.0f, 167.0f/255.0f, 11.0f/255.0f, 1.0f); // Ultrahaptics Green 2
    [Tooltip("The number of visualized control points")]
    public uint _historySize = 50;
    [Tooltip("The Sensation Source that is currently controlling emitter output")]
    public SensationSource CurrentSensation = null;

    int numberOfSensationEmittersInScene()
    {
        return FindObjectsOfType<SensationEmitter>().Length;
    }

    void Start ()
    {
        if (numberOfSensationEmittersInScene() > 1)
        {
            Debug.LogError("There is more than one Sensation Emitter in the scene. Please ensure there is only one Sensation Emitter in the scene.");
        }
    }

    private void OnEnable()
    {
        if (SensationCore.Instance != null && !SensationCore.Instance.IsEmitterConnected())
        {
            SensationCore.Instance.AcquireEmitter();
        }
    }

    void Update ()
    {
        if (SensationCore.Instance != null && !SensationCore.Instance.IsEmitterConnected())
        {
            SensationCore.Instance.AcquireEmitter();
        }
        if (SensationCore.Instance != null && SensationCore.Instance.IsEmitterConnected())
            SensationCore.Instance.SetEvaluationHistorySize(_historySize);
    }

    protected virtual void OnDisable()
    {
        if (SensationCore.Instance != null && SensationCore.Instance.IsEmitterConnected())
        {
                SensationCore.Instance.ReleaseEmitter();
        }
    }

    void OnDrawGizmos()
    {
        if(SensationCore.Instance != null && SensationCore.Instance.IsEmitterConnected())
            Render();
    }

    internal void Render()
    {
        if (SensationCore.Instance != null && SensationCore.Instance.IsEmitterConnected()) {
            var focalPoints = SensationCore.Instance.GetEvaluationHistory();
            if (focalPoints != null)
            {
                renderFocalPoints(focalPoints, _sphereColor, _sphereRadius);
            }
        }
    }

    void renderFocalPoints(List<UnityEngine.Vector4> focalPoints, Color color, float radius) {
        Color previousColor = Gizmos.color;
        Gizmos.color = color;

        foreach (var focalPoint in focalPoints)
        {
            var focalPointInEmitterSpace = new UnityEngine.Vector3(focalPoint.x, focalPoint.y, focalPoint.z);
            var focalPointInUnitySpace = UnityToEmitterSpace.Transform.inverse * focalPointInEmitterSpace;
            if (_arrayTransform != null)
            {
                focalPointInUnitySpace = _arrayTransform.TransformPoint(focalPointInUnitySpace);
            }
            color.a = focalPoint.w;
            Gizmos.color = color;
            Gizmos.DrawSphere(focalPointInUnitySpace, radius);
        }

        Gizmos.color = previousColor;
    }
}
}
