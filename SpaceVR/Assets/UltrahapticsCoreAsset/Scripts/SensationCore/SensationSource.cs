using System;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Assembly-CSharp-Editor")]

namespace Ultrahaptics
{
    [ExecuteInEditMode]
    public class SensationSource : MonoBehaviour
    {
        [Tooltip("The name of the Sensation Block used by this Sensation Source (e.g. Circle).\n\n" +
            "Sensation Blocks define the inputs and behaviour of mid-air haptic effects ('Sensations').\n\n" +
            "Sensation Blocks included with SensationCore library are described in BlockManifest.\n\n" +
            "New Sensations can be defined via the SensationCore Block Scripting API.")]
        [SerializeField] public string sensationBlock;

        [Tooltip("If enabled, the Sensation Source's Block Graph will be evaluated by SensationCore's playback engine.\n\n" +
            "If disabled, the Block Graph will not be evaluated and no haptics will be experienced.\n\n" +
            "Note: if multiple Sensations are 'Running' simultaneously, only the Sensation most recently set to be 'Running' will be evaluated.")]
        [SerializeField] protected bool _running = false;

        [Header("Inputs")]
        public SensationBlockInputs Inputs = new SensationBlockInputs();

        private const int INVALID_HANDLE = 0;

        [NonSerialized] protected int _handle = INVALID_HANDLE;
        [NonSerialized] private string _currentBlockName;

        protected string BlockName
        {
        	get { return sensationBlock != null ? sensationBlock.Trim() : null; }
        }
        protected virtual void LateUpdate ()
        {
            if (SensationCore.Instance != null && SensationCore.Instance.IsPlaying && Inputs != null)
            {
                if(_handle == INVALID_HANDLE)
                {
                    CheckAndCreateBlock();
                }
                if (BlockNameChanged())
                    OnValidate();
                if (_handle != INVALID_HANDLE)
                {
                    Inputs.ApplySaved(_handle);

                    UpdateSensation();
                }
            }
        }

        public void UpdateSensation()
        {
            try
            {
                SensationCore.Instance.CallUpdate();
            }
            catch (Exception)
            {
                Debug.LogWarning("Unable to update Sensation Source. Check your Sensation Block does not contain errors and your scene contains an enabled Sensation Emitter component.");
            }
        }

        public void RunForDuration(float seconds)
        {
            if (seconds < 0)
            {
                Debug.LogWarning("Playback Duration cannot be negative");
                return;
            }

            Running = true;
            StartCoroutine(StopRunningAfterSeconds(seconds));
        }

        private IEnumerator StopRunningAfterSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            Running = false;
        }

        private bool BlockNameChanged()
        {
            return _currentBlockName != BlockName;
        }

        private bool CurrentHandleIsValid()
        {
            return _handle > INVALID_HANDLE;
        }

        private void ReleaseCurrentBlock()
        {
            _handle = INVALID_HANDLE;
            // TODO: Release current handle
            StopPlayback();
        }

        internal void CheckAndCreateBlock()
        {
            bool useDefaults = false;
            if ((_currentBlockName != null && (_currentBlockName != BlockName)))
                useDefaults = true;
            CreateBlock(useDefaults);
        }
        internal void CreateBlock(bool useDefaults = false){
            _currentBlockName = BlockName;
            if (BlockName == null)
            {
                _handle = INVALID_HANDLE;
                return;
            }
            try
            {
                _handle = SensationCore.Instance.CreateBlock(BlockName);
                if(useDefaults)
                    GenerateInputs();
            }
            catch
            {
                _handle = INVALID_HANDLE;
                Inputs.Inputs.Clear();
            }
        }

        public void RecreateCurrentBlock()
        {
            ReleaseCurrentBlock();
            CreateBlock();
            StartPlayback();
        }

        protected int GetHandle()
        {
            if (BlockNameChanged() && !CurrentHandleIsValid()) { // This shouldn't be here...
                ReleaseCurrentBlock();
                CheckAndCreateBlock();
            }
            return _handle;
        }

        private void StartPlayback()
        {
            if (CurrentHandleIsValid() && SensationCore.Instance != null && SensationCore.Instance.IsPlaying && enabled)
            {
                if (_running)
                {
                    SensationCore.Instance.CallStart(_handle);
                }
                if(Inputs != null)
                    Inputs.ApplySaved(_handle);
            }
        }

        private void StopPlayback()
        {
            if (CurrentHandleIsValid() && SensationCore.Instance != null && SensationCore.Instance.IsPlaying)
            {
                SensationCore.Instance.Stop(_handle);
            }
        }

        protected virtual void OnEnable()
        {
            if (SensationCore.Instance == null)
                return;
            if (BlockNameChanged() || !CurrentHandleIsValid())
            {
                ReleaseCurrentBlock();
                CheckAndCreateBlock();
            }
            StartPlayback();
            SetRunning(Running);
        }

        protected virtual void OnDisable()
        {
            StopPlayback();
        }

        public bool Running {
            get { return _running; }
            set { SetRunning(value); }
        }

        public void Start()
        {
            if(Inputs != null)
            {
                Inputs.RegisterAutoMapper();
                StartPlayback();
                LateUpdate();
                SetRunning(Running);
                Inputs.RegisterInputs();
            }
        }

        private SensationEmitter _sensationEmitter = null;
        protected void SetRunning(bool running)
        {
            _running = running;
            // Only activate playback if the Unity Component is enabled.
            if(enabled && SensationCore.Instance != null && _handle > INVALID_HANDLE)
            {
                if (running)
                {
                    if (_sensationEmitter == null)
                        _sensationEmitter = GameObject.FindObjectOfType<SensationEmitter>();
                    if (_sensationEmitter != null)
                        _sensationEmitter.CurrentSensation = this;
                    SensationCore.Instance.Activate(_handle);
                }
                else
                {
                    SensationCore.Instance.Deactivate(_handle);
                }
            }
        }

        public void GenerateInputs()
        {
            Inputs.Reset();
            if (_handle == INVALID_HANDLE)
                _handle = GetHandle();
            if (_handle == INVALID_HANDLE)
                return;
            int inputSize = SensationCore.Instance.InputCount(_handle);
            for(int i = 0; i < inputSize; i++)
            {
                SensationBlockInput input = new SensationBlockInput
                {
                    name = SensationCore.Instance.InputName(_handle, i),
                    value = SensationCore.Instance.GetInput(_handle, i)
                };
                Inputs.AddInput(input);
            }
        }

        public virtual void OnValidate()
        {
            if (SensationCore.Instance == null)
            {
                return;
            }
            try
            {
                SetRunning(_running);
                if (BlockNameChanged() || !CurrentHandleIsValid())
                {
                    CheckAndCreateBlock();
                    StartPlayback();
                }
            }
            catch(DllNotFoundException)
            {
                // When re-importing the asset, scripts may be loaded before the native plugin
                // and OnValidate called on them, causing these spurious errors.
                // Functionality is not affected.
            }
        }
    }
}
