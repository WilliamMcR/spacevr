using System;
using System.Collections.Generic;

namespace Ultrahaptics {

public interface ILogStreamReader
{
    List<string> getInfoMessages(IntPtr sensationCoreInstancePtr);
    List<string> getWarningMessages(IntPtr sensationCoreInstancePtr);
    List<string> getErrorMessages(IntPtr sensationCoreInstancePtr);
}

}
