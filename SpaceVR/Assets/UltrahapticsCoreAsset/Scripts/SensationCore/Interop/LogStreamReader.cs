using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Ultrahaptics {

class LogStreamReader : ILogStreamReader
{
    private char logDelimiter = '\0';

    private ISensationCoreInterop _sensationCoreInterop;

    public LogStreamReader(ISensationCoreInterop sensationCoreInterop)
    {
        _sensationCoreInterop = sensationCoreInterop;
    }

    private List<string> getLogMessagesFromBuffer(byte[] buffer)
    {
        var logMessagesAsOneString = System.Text.Encoding.ASCII.GetString(buffer.Take(buffer.Length).ToArray());
        var logMessages = new List<string>(logMessagesAsOneString.Split(logDelimiter));
        logMessages.RemoveAt(logMessages.Count - 1);
        return logMessages;
    }


    delegate UIntPtr uhsclGetLogMessageBufferSize(IntPtr sensationCoreInstancePtr);
    delegate void uhsclGetLogMessageBufferAndClear(IntPtr sensationCoreInstancePtr, UIntPtr size, byte[] buffer);

    private List<string> getLogMessages(IntPtr sensationCoreInstancePtr,
                                        uhsclGetLogMessageBufferSize getBufferSize,
                                        uhsclGetLogMessageBufferAndClear getLogMessagesInBuffer)
    {
        var size = getBufferSize(sensationCoreInstancePtr);
        byte[] buffer = new byte[(int)size];
        getLogMessagesInBuffer(sensationCoreInstancePtr, size, buffer);

        return getLogMessagesFromBuffer(buffer);
    }

    public List<string> getInfoMessages(IntPtr sensationCoreInstancePtr)
    {
        return getLogMessages(
            sensationCoreInstancePtr,
            _sensationCoreInterop.uhsclGetLogInfoMessageBufferSize,
            _sensationCoreInterop.uhsclGetLogInfoMessageBufferAndClear);
    }

    public List<string> getWarningMessages(IntPtr sensationCoreInstancePtr)
    {
        return getLogMessages(
            sensationCoreInstancePtr,
            _sensationCoreInterop.uhsclGetLogWarningMessageBufferSize,
            _sensationCoreInterop.uhsclGetLogWarningMessageBufferAndClear);
    }

    public List<string> getErrorMessages(IntPtr sensationCoreInstancePtr)
    {
        return getLogMessages(
            sensationCoreInstancePtr,
            _sensationCoreInterop.uhsclGetLogErrorMessageBufferSize,
            _sensationCoreInterop.uhsclGetLogErrorMessageBufferAndClear);
    }
}

}
