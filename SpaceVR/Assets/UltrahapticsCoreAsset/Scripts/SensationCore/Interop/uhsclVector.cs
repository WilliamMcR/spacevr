﻿﻿using System.Runtime.InteropServices;
using UnityEngine;

namespace Ultrahaptics
{
    [StructLayout(LayoutKind.Sequential)]
    public struct uhsclVector3_t
    {
        public double x;
        public double y;
        public double z;

        public uhsclVector3_t(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public UnityEngine.Vector3 toVector3()
        {
            return new UnityEngine.Vector3 ((float)x, (float)y, (float)z);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct uhsclVector4_t
    {
        public double x;
        public double y;
        public double z;
        public double w;

        public uhsclVector4_t(double x, double y, double z, double w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public UnityEngine.Vector4 toVector4()
        {
            return new UnityEngine.Vector4((float)x, (float)y, (float)z, (float)w);
        }
    }
}
