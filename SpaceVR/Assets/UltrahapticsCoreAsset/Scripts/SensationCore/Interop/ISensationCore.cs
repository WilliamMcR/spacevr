﻿using System;
using System.Collections.Generic;
using Ultrahaptics;
using UnityEngine;

public interface ISensationCore : IDisposable
{
    bool IsPlaying { get; }
    int CreateBlock(string name);
    void CallStart(int handle);
    void Stop(int handle);
    void AcquireEmitter();
    bool IsEmitterConnected();
    void ReleaseEmitter();
    void Deactivate(int handle);
    void Activate(int handle);
    void CallUpdate();
    void SetEvaluationHistorySize(uint size);
    List<UnityEngine.Vector4> GetEvaluationHistory();
    int InputCount(int handle);
    string InputName(int handle, int idx);
    UnityEngine.Vector3 GetInput(int handle, int inputIdx);
    void SetInput(int handle, int inputIdx, UnityEngine.Vector3 inputValue);
    void AddBlockSearchPath(string path, bool streamingAssetsPath = false);
    void CreateSensationCore(ILogStreamReader logStreamReader = null);
    void CheckCall(uhsclErrorCode_t noError);
}
