using System;
using System.Runtime.InteropServices;

namespace Ultrahaptics
{
    public interface ISensationCoreInterop
    {
        IntPtr uhsclCreate();

        void uhsclRelease(IntPtr sensationCoreInstancePtr);

        uhsclErrorCode_t uhsclImportPythonModule(
            IntPtr instance,
            string modulename
        );

        uhsclErrorCode_t uhsclGetNameLength(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            ref UInt32 count
        );

        uhsclErrorCode_t uhsclGetName(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            Int32 nameBufferLength,
            [In, Out] byte[] nameBuffer
        );

        uhsclErrorCode_t uhsclCreateBlock(
            IntPtr sensationCoreInstancePtr,
            string identifier,
            ref Int32 handle
        );

        uhsclErrorCode_t uhsclStart(
            IntPtr sensationCoreInstancePtr,
            Int32 handle
        );

        uhsclErrorCode_t uhsclStop(
            IntPtr sensationCoreInstancePtr,
            Int32 handle
        );

        uhsclErrorCode_t uhsclAcquireEmitter(
            IntPtr sensationCoreInstancePtr
        );

        Int32 uhsclIsEmitterConnected(
            IntPtr sensationCoreInstancePtr
        );

        uhsclErrorCode_t uhsclReleaseEmitter(
            IntPtr sensationCoreInstancePtr
        );

        uhsclErrorCode_t uhsclDeactivate(
            IntPtr sensationCoreInstancePtr,
            Int32 handle
        );

        uhsclErrorCode_t uhsclActivate(
            IntPtr sensationCoreInstancePtr,
            Int32 handle
        );

        uhsclErrorCode_t uhsclInputCount(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            ref Int32 count
        );

        uhsclErrorCode_t uhsclGetInputAtIndex(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            Int32 idx,
            ref Int32 inputHandle
        );

        uhsclErrorCode_t uhsclSetInput(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            Int32 idx,
            uhsclVector3_t inputVector
        );

        uhsclErrorCode_t uhsclGetInput(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            Int32 idx,
            ref uhsclVector3_t value
        );

        uhsclErrorCode_t uhsclUpdate(
            IntPtr sensationCoreInstancePtr
        );

        uhsclErrorCode_t uhsclSetEvaluationHistorySizeOnCurrentPlaybackDevice(
            IntPtr sensationCoreInstancePtr,
            UInt32 size
        );

        uhsclErrorCode_t uhsclGetEvaluationHistoryOnCurrentPlaybackDevice(
            IntPtr sensationCoreInstancePtr,
            [In, Out] uhsclVector4_t[] evaluationHistory,
            ref UInt32 size
        );

        uhsclErrorCode_t uhsclAddSearchPath(
            IntPtr sensationCoreInstancePtr,
            string path
        );

        uhsclErrorCode_t uhsclGetOutputByName(
            IntPtr sensationCoreInstancePtr,
            Int32 blockDefinitionHandle,
            string outputName,
            ref Int32 blockOutputHandle
        );

        uhsclErrorCode_t uhsclGetInputByName(
            IntPtr sensationCoreInstancePtr,
            Int32 blockDefinitionHandle,
            string inputName,
            ref Int32 blockInputHandle
        );

        uhsclErrorCode_t uhsclEvaluateBlock(
            IntPtr sensationCoreInstancePtr,
            Int32 blockHandle,
            ref uhsclVector3_t value,
            ref uhsclVector3_t inputAsVectors
        );

        uhsclErrorCode_t uhsclImportBlock(
            IntPtr sensationCoreInstancePtr,
            string identifier,
            ref Int32 blockHandle
        );

        UIntPtr uhsclGetLogInfoMessageBufferSize(IntPtr sensationCoreInstancePtr);
        UIntPtr uhsclGetLogWarningMessageBufferSize(IntPtr sensationCoreInstancePtr);
        UIntPtr uhsclGetLogErrorMessageBufferSize(IntPtr sensationCoreInstancePtr);

        void uhsclGetLogInfoMessageBufferAndClear(
            IntPtr sensationCoreInstancePtr,
            UIntPtr logBufferLength,
            [In, Out] byte[] logBuffer
        );

        void uhsclGetLogWarningMessageBufferAndClear(
            IntPtr sensationCoreInstancePtr,
            UIntPtr logBufferLength,
            [In, Out] byte[] logBuffer
        );

        void uhsclGetLogErrorMessageBufferAndClear(
            IntPtr sensationCoreInstancePtr,
            UIntPtr logBufferLength,
            [In, Out] byte[] logBuffer
        );

    }
}
