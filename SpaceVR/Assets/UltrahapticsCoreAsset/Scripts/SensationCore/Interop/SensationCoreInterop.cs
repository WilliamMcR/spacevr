using System;
using System.Runtime.InteropServices;

namespace Ultrahaptics
{
    internal class StringMarshaler : ICustomMarshaler
    {
        private static readonly System.Collections.Generic.HashSet<IntPtr> owned = new System.Collections.Generic.HashSet<IntPtr>();

        public void CleanUpManagedData(object ManagedObj)
        {
        }

        public void CleanUpNativeData(IntPtr pNativeData)
        {
            if (owned.Remove(pNativeData)) Marshal.FreeCoTaskMem(pNativeData);
        }

        public int GetNativeDataSize()
        {
            throw new NotImplementedException();
        }

        public IntPtr MarshalManagedToNative(object ManagedObj)
        {
            var ptr = Marshal.StringToCoTaskMemAnsi((string)ManagedObj);
            owned.Add(ptr);
            return ptr;
        }

        public object MarshalNativeToManaged(IntPtr pNativeData)
        {
            return Marshal.PtrToStringAnsi(pNativeData);
        }

        public static ICustomMarshaler GetInstance(string cookie)
        {
            return new StringMarshaler();
        }
    }

    public class SensationCoreInterop : ISensationCoreInterop
    {
        private class Native
        {

            [DllImport("SensationCore")]
            internal static extern IntPtr uhsclCreate();

            [DllImport("SensationCore")]
            internal static extern void uhsclRelease(IntPtr sensationCoreInstancePtr);

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclImportPythonModule(
                IntPtr sensationCoreInstancePtr,
                [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string modulename
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclGetNameLength(
                IntPtr sensationCoreInstancePtr,
                Int32 handle,
                ref UInt32 count
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclGetName(
                IntPtr sensationCoreInstancePtr,
                Int32 handle,
                Int32 nameBufferLength,
                [In, Out] byte[] nameBuffer
            );
            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclCreateBlock(
                IntPtr sensationCoreInstancePtr,
                [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string identifier,
                ref Int32 handle
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclStart(
                IntPtr sensationCoreInstancePtr,
                Int32 handle
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclStop(
                IntPtr sensationCoreInstancePtr,
                Int32 handle
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclAcquireEmitter(
                IntPtr sensationCoreInstancePtr
            );

            [DllImport("SensationCore")]
            internal static extern Int32 uhsclIsEmitterConnected(
                IntPtr sensationCoreInstancePtr
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclReleaseEmitter(
                IntPtr sensationCoreInstancePtr
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclDeactivate(
                IntPtr sensationCoreInstancePtr,
                Int32 handle
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclActivate(
                IntPtr sensationCoreInstancePtr,
                Int32 handle
            );
            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclInputCount(
                IntPtr sensationCoreInstancePtr,
                Int32 handle,
                ref Int32 count
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclGetInputAtIndex(
                IntPtr sensationCoreInstancePtr,
                Int32 handle,
                Int32 idx,
                ref Int32 inputHandle
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclSetInput(
                IntPtr sensationCoreInstancePtr,
                Int32 handle,
                Int32 idx,
                uhsclVector3_t inputVector
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclGetInput(
                IntPtr sensationCoreInstancePtr,
                Int32 handle,
                Int32 idx,
                ref uhsclVector3_t value
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclUpdate(
                IntPtr sensationCoreInstancePtr
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclSetEvaluationHistorySizeOnCurrentPlaybackDevice(
                IntPtr sensationCoreInstancePtr,
                UInt32 size
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclGetEvaluationHistoryOnCurrentPlaybackDevice(
                IntPtr sensationCoreInstancePtr,
                [In, Out] uhsclVector4_t[] evaluationHistory,
                ref UInt32 size
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclAddSearchPath(
                IntPtr sensationCoreInstancePtr,
                [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string path
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclGetOutputByName(
                IntPtr sensationCoreInstancePtr,
                Int32 blockDefinitionHandle,
                [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string outputName,
                ref Int32 blockOutputHandle
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclGetInputByName(
                IntPtr sensationCoreInstancePtr,
                Int32 blockDefinitionHandle,
                [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string inputName,
                ref Int32 blockInputHandle
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclEvaluateBlock(
                IntPtr sensationCoreInstancePtr,
                Int32 blockHandle,
                ref uhsclVector3_t value,
                ref uhsclVector3_t inputAsVectors
            );

            [DllImport("SensationCore")]
            internal static extern uhsclErrorCode_t uhsclImportBlock(
                IntPtr sensationCoreInstancePtr,
                [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef=typeof(StringMarshaler))] string identifier,
                ref Int32 blockHandle
            );

            [DllImport("SensationCore")]
            internal static extern UIntPtr uhsclGetLogInfoMessageBufferSize(IntPtr sensationCoreInstancePtr);

            [DllImport("SensationCore")]
            internal static extern UIntPtr uhsclGetLogWarningMessageBufferSize(IntPtr sensationCoreInstancePtr);

            [DllImport("SensationCore")]
            internal static extern UIntPtr uhsclGetLogErrorMessageBufferSize(IntPtr sensationCoreInstancePtr);

            [DllImport("SensationCore")]
            internal static extern void uhsclGetLogInfoMessageBufferAndClear(
                IntPtr sensationCoreInstancePtr,
                UIntPtr logBufferLength,
                [In, Out] byte[] logBuffer
            );

            [DllImport("SensationCore")]
            internal static extern void uhsclGetLogWarningMessageBufferAndClear(
                IntPtr sensationCoreInstancePtr,
                UIntPtr logBufferLength,
                [In, Out] byte[] logBuffer
            );

            [DllImport("SensationCore")]
            internal static extern void uhsclGetLogErrorMessageBufferAndClear(
                IntPtr sensationCoreInstancePtr,
                UIntPtr logBufferLength,
                [In, Out] byte[] logBuffer
            );
        }

        public IntPtr uhsclCreate()
        {
            return Native.uhsclCreate();
        }


        public void uhsclRelease(IntPtr sensationCoreInstancePtr)
        {
            Native.uhsclRelease(sensationCoreInstancePtr);
        }

        public uhsclErrorCode_t uhsclImportPythonModule(
            IntPtr sensationCoreInstancePtr,
            string modulename
            )
        {
            return Native.uhsclImportPythonModule(sensationCoreInstancePtr, modulename);
        }

        public uhsclErrorCode_t uhsclGetNameLength(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            ref UInt32 count
            )
        {
            return Native.uhsclGetNameLength(sensationCoreInstancePtr, handle, ref count);
        }

        public uhsclErrorCode_t uhsclGetName(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            Int32 nameBufferLength,
            [In, Out] byte[] nameBuffer
            )
        {
            return Native.uhsclGetName(sensationCoreInstancePtr, handle, nameBufferLength, nameBuffer);
        }

        public uhsclErrorCode_t uhsclCreateBlock(
            IntPtr sensationCoreInstancePtr,
            string identifier,
            ref Int32 handle
            )
        {
            return Native.uhsclCreateBlock(sensationCoreInstancePtr, identifier, ref handle);
        }

        public uhsclErrorCode_t uhsclStart(
            IntPtr sensationCoreInstancePtr,
            Int32 handle
            )
        {
            return Native.uhsclStart(sensationCoreInstancePtr, handle);
        }

        public uhsclErrorCode_t uhsclStop(
            IntPtr sensationCoreInstancePtr,
            Int32 handle
            )
        {
            return Native.uhsclStop(sensationCoreInstancePtr, handle);
        }

        public uhsclErrorCode_t uhsclAcquireEmitter(
            IntPtr sensationCoreInstancePtr
            )
        {
            return Native.uhsclAcquireEmitter(sensationCoreInstancePtr);
        }

        public Int32 uhsclIsEmitterConnected(
            IntPtr sensationCoreInstancePtr
            )
        {
            return Native.uhsclIsEmitterConnected(sensationCoreInstancePtr);
        }

        public uhsclErrorCode_t uhsclReleaseEmitter(
            IntPtr sensationCoreInstancePtr
        )
        {
            return Native.uhsclReleaseEmitter(sensationCoreInstancePtr);
        }

        public uhsclErrorCode_t uhsclDeactivate(
            IntPtr sensationCoreInstancePtr,
            Int32 handle
        )
        {
            return Native.uhsclDeactivate(sensationCoreInstancePtr, handle);
        }

        public uhsclErrorCode_t uhsclActivate(
            IntPtr sensationCoreInstancePtr,
            Int32 handle
        )
        {
            return Native.uhsclActivate(sensationCoreInstancePtr, handle);
        }

        public uhsclErrorCode_t uhsclInputCount(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            ref Int32 count
        )
        {
            return Native.uhsclInputCount(sensationCoreInstancePtr, handle, ref count);
        }

        public uhsclErrorCode_t uhsclGetInputAtIndex(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            Int32 idx,
            ref Int32 inputHandle
        )
        {
            return Native.uhsclGetInputAtIndex(sensationCoreInstancePtr, handle, idx, ref inputHandle);
        }

        public uhsclErrorCode_t uhsclSetInput(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            Int32 idx,
            uhsclVector3_t inputVector
        )
        {
            return Native.uhsclSetInput(sensationCoreInstancePtr, handle, idx, inputVector);
        }

        public uhsclErrorCode_t uhsclGetInput(
            IntPtr sensationCoreInstancePtr,
            Int32 handle,
            Int32 idx,
            ref uhsclVector3_t value
        )
        {
            return Native.uhsclGetInput(sensationCoreInstancePtr, handle, idx, ref value);
        }

        public uhsclErrorCode_t uhsclUpdate(
            IntPtr sensationCoreInstancePtr
        )
        {
            return Native.uhsclUpdate(sensationCoreInstancePtr);
        }

        public uhsclErrorCode_t uhsclSetEvaluationHistorySizeOnCurrentPlaybackDevice(
            IntPtr sensationCoreInstancePtr,
            UInt32 size
        )
        {
            return Native.uhsclSetEvaluationHistorySizeOnCurrentPlaybackDevice(sensationCoreInstancePtr, size);
        }

        public uhsclErrorCode_t uhsclGetEvaluationHistoryOnCurrentPlaybackDevice(
            IntPtr sensationCoreInstancePtr,
            [In, Out] uhsclVector4_t[] evaluationHistory,
            ref UInt32 size
        )
        {
            return Native.uhsclGetEvaluationHistoryOnCurrentPlaybackDevice(sensationCoreInstancePtr, evaluationHistory, ref size);
        }

        public uhsclErrorCode_t uhsclAddSearchPath(
            IntPtr sensationCoreInstancePtr,
            string path
        )
        {
            return Native.uhsclAddSearchPath(sensationCoreInstancePtr, path);
        }

        public uhsclErrorCode_t uhsclGetOutputByName(
            IntPtr sensationCoreInstancePtr,
            Int32 blockDefinitionHandle,
            string outputName,
            ref Int32 blockOutputHandle
        )
        {
            return Native.uhsclGetOutputByName(sensationCoreInstancePtr, blockDefinitionHandle, outputName, ref blockOutputHandle);
        }

        public uhsclErrorCode_t uhsclGetInputByName(
            IntPtr sensationCoreInstancePtr,
            Int32 blockDefinitionHandle,
            string inputName,
            ref Int32 blockInputHandle
        )
        {
            return Native.uhsclGetInputByName(sensationCoreInstancePtr, blockDefinitionHandle, inputName, ref blockInputHandle);
        }

        public uhsclErrorCode_t uhsclEvaluateBlock(
            IntPtr sensationCoreInstancePtr,
            Int32 blockHandle,
            ref uhsclVector3_t value,
            ref uhsclVector3_t inputAsVectors
        )
        {
            return Native.uhsclEvaluateBlock(sensationCoreInstancePtr, blockHandle, ref value, ref inputAsVectors);
        }

        public uhsclErrorCode_t uhsclImportBlock(
            IntPtr sensationCoreInstancePtr,
            string identifier,
            ref Int32 blockHandle
        )
        {
            return Native.uhsclImportBlock(sensationCoreInstancePtr, identifier, ref blockHandle);
        }

        public UIntPtr uhsclGetLogInfoMessageBufferSize(IntPtr sensationCoreInstancePtr)
        {
            return Native.uhsclGetLogInfoMessageBufferSize(sensationCoreInstancePtr);
        }

        public UIntPtr uhsclGetLogWarningMessageBufferSize(IntPtr sensationCoreInstancePtr)
        {
            return Native.uhsclGetLogWarningMessageBufferSize(sensationCoreInstancePtr);
        }

        public UIntPtr uhsclGetLogErrorMessageBufferSize(IntPtr sensationCoreInstancePtr)
        {
            return Native.uhsclGetLogErrorMessageBufferSize(sensationCoreInstancePtr);
        }

        public void uhsclGetLogInfoMessageBufferAndClear(
            IntPtr sensationCoreInstancePtr,
            UIntPtr logBufferLength,
            [In, Out] byte[] logBuffer
        )
        {
            Native.uhsclGetLogInfoMessageBufferAndClear(sensationCoreInstancePtr, logBufferLength, logBuffer);
        }

        public void uhsclGetLogWarningMessageBufferAndClear(
            IntPtr sensationCoreInstancePtr,
            UIntPtr logBufferLength,
            [In, Out] byte[] logBuffer
        )
        {
            Native.uhsclGetLogWarningMessageBufferAndClear(sensationCoreInstancePtr, logBufferLength, logBuffer);
        }

        public void uhsclGetLogErrorMessageBufferAndClear(
            IntPtr sensationCoreInstancePtr,
            UIntPtr logBufferLength,
            [In, Out] byte[] logBuffer
        )
        {
            Native.uhsclGetLogErrorMessageBufferAndClear(sensationCoreInstancePtr, logBufferLength, logBuffer);
        }

    }
}
