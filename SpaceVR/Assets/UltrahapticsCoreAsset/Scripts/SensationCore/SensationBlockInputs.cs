using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
namespace Ultrahaptics
{
    [Serializable]
    public class SensationBlockInputs
    {
        public List<SensationBlockInput> Inputs = new List<SensationBlockInput>();
        [NonSerialized] private AutoMapper _autoMapper;

        public void ApplySaved(int handle)
        {
            for (int i = 0; i < Inputs.Count; i++)
            {
                SensationCore.Instance.SetInput(handle, i, Inputs[i].value);
            }
        }


        public UnityEngine.Vector3 this[string name]
        {
            get
            {
                foreach(var input in Inputs)
                {
                    if (input.name == name)
                        return input.value;
                }
                return UnityEngine.Vector3.zero;
             }
            set
            {
                bool found = false;
                foreach(var input in Inputs)
                {
                    if (input.name == name)
                    {
                        found = true;
                        input.value = value;
                    }
                }
                if (!found)
                {
                    Debug.LogError("Tried to set property on invalid input " + name);
                }
            }
        }


        internal void AddInput(SensationBlockInput input)
        {
            Inputs.Add(input);
            if (_autoMapper)
            {
                _autoMapper.RegisterBlockInput(input);
            }
        }


        internal void Reset()
        {
            if (_autoMapper)
            {
                foreach (var input in Inputs)
                {
                    _autoMapper.DeregisterBlockInput(input);
                }
            }
            Inputs.Clear();
        }

        internal void RegisterAutoMapper()
        {
            if (!_autoMapper)
            {
                _autoMapper = GameObject.FindObjectOfType<AutoMapper>();
            }
        }

        internal void RegisterInputs()
        {
            if (_autoMapper)
            {
                foreach (var input in Inputs)
                {
                    _autoMapper.RegisterBlockInput(input);
                }
            }
        }

        internal string GetName(int handle, int index)
        {
            return SensationCore.Instance.InputName(handle, index);
        }
    }
}
