﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Collections;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor.Callbacks;
using UnityEditor;
#endif

namespace Ultrahaptics
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    public class SensationCore : MonoBehaviour, ISensationCore
    {
        public static string DefaultSearchPath = "Python";

        private ISensationCoreInterop _sensationCoreInterop;
        private ILogStreamReader _logStreamReader;
        private IntPtr _sensationCoreInstancePtr;
        private uint _evaluationHistorySize = 0;
        private List<string> _searchPaths = new List<string>();

        public static ISensationCore Instance = null;

        private object _coreLock = new object();

        public void Start()
        {
            CreateSensationCore();
#if UNITY_EDITOR
            AssemblyReloadEvents.beforeAssemblyReload += OnDestroy;
#endif
        }

#if UNITY_EDITOR
        [DidReloadScripts(1)]
        internal static void ScriptReload()
        {
            if (!EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode)
            {
                return;
            }
            if (Instance == null)
            {
                SensationCore core = GameObject.FindObjectOfType<SensationCore>();
                if(core != null)
                {
                    core.Start();
                }
            }
        }
#endif
        public void CreateSensationCore(ILogStreamReader logStreamReader = null)
        {
            lock (_coreLock)
            {
                try
                {
                    if (SensationCore.Instance == null)
                    {
                        _sensationCoreInterop = new SensationCoreInterop();
                        if (logStreamReader == null)
                            _logStreamReader = new LogStreamReader(_sensationCoreInterop);
                        else
                            _logStreamReader = logStreamReader;
                        _sensationCoreInstancePtr = _sensationCoreInterop.uhsclCreate();
                        if (_sensationCoreInstancePtr == default(IntPtr))
                        {
                            throw new Exception("Failed to initialise SensationCore library");
                        }
                        // Default Python Block Search Path is StreamingAssets/Python
                        AddBlockSearchPath(DefaultSearchPath, true);
                        ValidateSearchPaths();
                        Instance = this;
                        if(Application.isPlaying)
                            Debug.Log("SensationCore Successfully Created");
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("Exception creating SensationCore: " + e);
                }
            }
        }

#if UNITY_EDITOR
        public void OnDisable()
        {
            if (!EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode)
            {
                OnDestroy();
            }
        }
#endif

        public void OnDestroy()
        {
            lock(_coreLock)
            {
                try
                {
                    if(_sensationCoreInterop != null)
                    {
                        ReleaseEmitter();
                        _sensationCoreInterop.uhsclRelease(_sensationCoreInstancePtr);
                        _sensationCoreInterop = null;
                        SensationCore.Instance = null;
                        _sensationCoreInstancePtr = default(IntPtr);
                    }
                } catch(Exception e)
                {
                    Debug.Log("Exception when destroying SensationCore: " + e);
                }
            }
        }

        public virtual bool IsPlaying
        {
            get { return Application.isPlaying; }
        }

        virtual public int CreateBlock(string name)
        {
            int handle=0;
            if(_sensationCoreInterop != null)
            {
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclCreateBlock(_sensationCoreInstancePtr, name, ref handle));
                    return handle;
                }
            }
            return 0;
        }

        virtual public void CallStart(int handle)
        {
            if (_sensationCoreInterop != null)
            {
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclStart(_sensationCoreInstancePtr, handle));
                }
            }
        }

        virtual public void Stop(int handle)
        {
            if (_sensationCoreInterop != null)
            {
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclStop(_sensationCoreInstancePtr, handle));
                }
            }
        }

        virtual public void AcquireEmitter()
        {
            if (_sensationCoreInterop != null)
            {
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclAcquireEmitter(_sensationCoreInstancePtr));
                }
            }
        }

        virtual public bool IsEmitterConnected()
        {
            if (_sensationCoreInterop != null)
            {
                lock (_coreLock)
                {
                    if (_sensationCoreInterop.uhsclIsEmitterConnected(_sensationCoreInstancePtr) != 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        virtual public void ReleaseEmitter()
        {
            if (_sensationCoreInterop != null)
            {
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclReleaseEmitter(_sensationCoreInstancePtr));
                }
            }
        }

        virtual public void Deactivate(int handle)
        {
            if (_sensationCoreInterop != null)
            {
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclDeactivate(_sensationCoreInstancePtr, handle));
                }
            }
        }
        virtual public void Activate(int handle)
        {
            if (_sensationCoreInterop != null)
            {
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclActivate(_sensationCoreInstancePtr, handle));
                }
            }
        }

        public void Update()
        {
            if (_sensationCoreInterop != null)
            {
                lock (_coreLock){
                    if(IsEmitterConnected())
                        CheckCall(_sensationCoreInterop.uhsclUpdate(_sensationCoreInstancePtr));
                }
            }
        }

        virtual public void SetEvaluationHistorySize(uint size)
        {
            if (_sensationCoreInterop != null)
            {
                _evaluationHistorySize = size;
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclSetEvaluationHistorySizeOnCurrentPlaybackDevice(_sensationCoreInstancePtr, size));
                }
            }
        }

        virtual public List<Vector4> GetEvaluationHistory()
        {
            List<Vector4> evaluationHistoryUnityVector4 = new List<Vector4>();
            if (_sensationCoreInterop != null)
            {

                uhsclVector4_t[] evaluationHistoryUhsclVector4 = new uhsclVector4_t[_evaluationHistorySize];
                uint size = 0;
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclGetEvaluationHistoryOnCurrentPlaybackDevice(_sensationCoreInstancePtr, evaluationHistoryUhsclVector4, ref size));
                }
                for(int i = 0; i < size; i++)
                {
                    evaluationHistoryUnityVector4.Add(evaluationHistoryUhsclVector4[i].toVector4());
                }
            }
            return evaluationHistoryUnityVector4;
        }

        virtual public int InputCount(int handle)
        {
           int count = 0;
            if (_sensationCoreInterop != null)
            {
                // We expect to see invalid handle ect. here so we don't catch the implicitly thrown error
                // Instead we return the closest thing to a default input count - 0.
                // TODO: Unify the interface to ISensationCore so invalid requests throw errors instead of defaulting values
                lock (_coreLock)
                {
                    _sensationCoreInterop.uhsclInputCount(_sensationCoreInstancePtr, handle, ref count);
                }
            }
            return count;
        }

        virtual public string InputName(int handle, int idx)
        {
            if (_sensationCoreInterop != null)
            {
                int inputHandle = 0;
                uint len = 0;
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclGetInputAtIndex(_sensationCoreInstancePtr, handle, idx, ref inputHandle));
                    CheckCall(_sensationCoreInterop.uhsclGetNameLength(_sensationCoreInstancePtr, inputHandle, ref len));
                    byte[] buf = new byte[len+1];
                    CheckCall(_sensationCoreInterop.uhsclGetName(_sensationCoreInstancePtr, inputHandle, Convert.ToInt32(len+1), buf));
                    // Drop the null terminating character when creating the C# string
                    return System.Text.Encoding.ASCII.GetString(buf.Take(buf.Length-1).ToArray());
                }
            }
            return null;
        }

        virtual public UnityEngine.Vector3 GetInput(int handle, int inputIdx)
        {
           uhsclVector3_t value = new uhsclVector3_t(0,0,0);
            if (_sensationCoreInterop != null)
            {
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclGetInput(_sensationCoreInstancePtr, handle, inputIdx, ref value));
                }
            }
            return value.toVector3();
        }

        virtual public void SetInput(int handle, int inputIdx, UnityEngine.Vector3 inputValue)
        {
            if (_sensationCoreInterop != null)
            {
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclSetInput(_sensationCoreInstancePtr, handle, inputIdx, new uhsclVector3_t(inputValue.x, inputValue.y, inputValue.z)));
                }
            }
        }

        public void AddBlockSearchPath(string path, bool streamingAssetsPath=false)
        {
            if (_sensationCoreInterop != null)
            {
                var fullPath = path;
                if (streamingAssetsPath){
                    fullPath = Application.streamingAssetsPath + "/" + path;
                }
                else {
                    fullPath = Application.dataPath + "/" + path;
                }

                if(!Directory.Exists(fullPath))
                {
                    if (path != DefaultSearchPath)
                    {
                      Debug.LogWarning(string.Format("The custom search path directory \"{0}\" could not be found", path));
                    }
                    return;
                }

                _searchPaths.Add(fullPath);
                lock (_coreLock)
                {
                    CheckCall(_sensationCoreInterop.uhsclAddSearchPath(_sensationCoreInstancePtr, fullPath));
                }
                ImportPythonFilesInPath(fullPath);
            }
        }

        private void ImportPythonFilesInPath(string path)
        {
            if (_sensationCoreInterop != null)
            {
                lock (_coreLock)
                {
                    foreach (string file in System.IO.Directory.GetFiles(path))
                    {
                        if (Path.GetExtension (file) == ".py")
                        {
                            var module = Path.GetFileNameWithoutExtension (file);
                            Console.Error.WriteLine ("Loading " + module + "...");
                            _sensationCoreInterop.uhsclImportPythonModule (_sensationCoreInstancePtr, module);
                        }
                    }
                }
            }
        }

        private bool NoSearchPathsExist()
        {
            return !_searchPaths.Any(path => Directory.Exists(path));
        }

        private void ValidateSearchPaths()
        {
            if(NoSearchPathsExist())
            {
                Debug.LogWarning("No valid search paths for Sensation Blocks could be found.\n" +
                                 "Please check that 'Assets/StreamingAssets/Python' directory exists, " +
                                 "or your custom search paths exist in the project.");
            }
        }

        private void ForwardLogMessagesToUnity()
        {
            var info = _logStreamReader.getInfoMessages(_sensationCoreInstancePtr);
            if (info != null)
            {
                info.ForEach(x => Debug.Log(x));
            }

            var warning = _logStreamReader.getWarningMessages(_sensationCoreInstancePtr);
            if (warning != null)
            {
                warning.ForEach(x => Debug.LogWarning(x));
            }

            var error = _logStreamReader.getErrorMessages(_sensationCoreInstancePtr);
            if (error != null)
            {
                error.ForEach(x => Debug.LogError(x));
            }
        }

        private void ThrowError(uhsclErrorCode_t errorCode)
        {
            switch (errorCode) {
                case uhsclErrorCode_t.NoError:
                    break;
                case uhsclErrorCode_t.UninitialisedSensationCore:
                    throw new Exception ("SensationCore not initialised");
                case uhsclErrorCode_t.InvalidHandle:
                    throw new ArgumentException ("Invalid handle");
                case uhsclErrorCode_t.InvalidArgument:
                    throw new ArgumentException ("Invalid argument");
                case uhsclErrorCode_t.InvalidOperation:
                    throw new InvalidOperationException ("Invalid operation");
                case uhsclErrorCode_t.HardwareError:
                    throw new IOException ("Hardware Error");
                default:
                    throw new Exception ("Unknown error: " + (int)errorCode);
            }
        }

        public void CheckCall(uhsclErrorCode_t errorCode)
        {
            if (_logStreamReader != null)
            {
                ForwardLogMessagesToUnity();
            }
            if (errorCode != uhsclErrorCode_t.NoError)
            {
                ThrowError(errorCode);
            }
        }

        public void CallUpdate()
        {
            Update();
        }

        public void Dispose()
        {
            OnDestroy();
        }
    }
}
