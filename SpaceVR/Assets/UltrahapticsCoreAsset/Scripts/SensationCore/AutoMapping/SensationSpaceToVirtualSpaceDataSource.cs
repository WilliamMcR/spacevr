﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Ultrahaptics
{

public class SensationSpaceToVirtualSpaceDataSource : MonoBehaviour, IDataSource
{
    private Dictionary<string, UnityEngine.Vector3> _data = new Dictionary<string, UnityEngine.Vector3>();

    public string[] GetAvailableDataItemsForType<T>()
    {
        if (typeof(T) == typeof(UnityEngine.Vector3))
        {
            return _data.Keys.ToArray();
        }
        return null;
    }

    public T GetDataItemByName<T>(string name)
    {
        if (!_data.ContainsKey(name))
        {
            throw new NullReferenceException();
        }
        return (T)(object)_data[name]; // Force conversion from Vector3 to T (which must be Vector3)
    }

    int numberOfDataSourceInScene()
    {
        return GameObject.FindObjectsOfType<SensationSpaceToVirtualSpaceDataSource>().Length;
    }

    void Start ()
    {
        _data.Add("sensationOriginInVirtualSpace", new UnityEngine.Vector3(0, 0, 0));
        _data.Add("sensationXInVirtualSpace", new UnityEngine.Vector3(1, 0, 0));
        _data.Add("sensationYInVirtualSpace", new UnityEngine.Vector3(0, 0, 1));
        _data.Add("sensationZInVirtualSpace", new UnityEngine.Vector3(0, 1, 0));

        this.RegisterToAutoMapper();
        if (numberOfDataSourceInScene() > 1)
        {
            this.LogWarningForDuplicateDataSources("Sensation Space to Virtual Space Data Source");
        }
    }

}

}
