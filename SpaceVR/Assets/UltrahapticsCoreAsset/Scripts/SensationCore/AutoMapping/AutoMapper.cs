using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace Ultrahaptics
{
    public class AutoMapper : MonoBehaviour
    {
        private List<IDataSource> _sources = new List<IDataSource>();
        private List<SensationBlockInput> _inputs = new List<SensationBlockInput>();

        public virtual void RegisterDataSource(IDataSource dataSource)
        {
            _sources.Add(dataSource);
        }

        public virtual void RegisterBlockInput(SensationBlockInput blockInput)
        {
            _inputs.Add(blockInput);
        }

        public virtual void DeregisterDataSource(IDataSource dataSource)
        {
            _sources.Remove(dataSource);
        }

        public virtual void DeregisterBlockInput(SensationBlockInput blockInput)
        {
            _inputs.Remove(blockInput);
        }

        public virtual void Update()
        {
            foreach (var source in _sources)
            {
                foreach (var input in _inputs)
                {
                    var availableDataItems = source.GetAvailableDataItemsForType<UnityEngine.Vector3>();
                    if(availableDataItems.Any(dataItem => dataItem == input.name))
                    {
                        input.value = source.GetDataItemByName<UnityEngine.Vector3>(input.name);
                    }
                }
            }
        }
    }
}
