﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Ultrahaptics
{

public class EmitterDataSource : MonoBehaviour, IDataSource
{
    private Dictionary<string, UnityEngine.Vector3> _data = new Dictionary<string, UnityEngine.Vector3>();

    public string[] GetAvailableDataItemsForType<T>()
    {
        if (typeof(T) == typeof(UnityEngine.Vector3))
        {
            return _data.Keys.ToArray();
        }
        return null;
    }

    public T GetDataItemByName<T>(string name)
    {
        if (!_data.ContainsKey(name))
        {
            throw new NullReferenceException();
        }
        return (T)(object)_data[name]; // Force conversion from Vector3 to T (which must be Vector3)
    }

    UnityEngine.Vector3 getPosition()
    {
        return this.gameObject.transform.position;
    }

    UnityEngine.Vector3 getXAxis()
    {
        var rotation = this.gameObject.transform.rotation;
        var direction = new UnityEngine.Vector3(1, 0, 0);
        return rotation * direction;
    }

    UnityEngine.Vector3 getYAxis()
    {
        var rotation = this.gameObject.transform.rotation;
        var direction = new UnityEngine.Vector3(0, 1, 0);
        return rotation * direction;
    }

    UnityEngine.Vector3 getZAxis()
    {
        var rotation = this.gameObject.transform.rotation;
        var direction = new UnityEngine.Vector3(0, 0, 1);
        return rotation * direction;
    }

    int numberOfDataSourceInScene()
    {
        return GameObject.FindObjectsOfType<EmitterDataSource>().Length;
    }

    void updateDataItems()
    {
        _data["virtualEmitterOriginInVirtualSpace"] = getPosition();
        _data["virtualEmitterXInVirtualSpace"] = getXAxis();
        _data["virtualEmitterYInVirtualSpace"] = getYAxis();
        _data["virtualEmitterZInVirtualSpace"] = getZAxis();
    }

    void Start()
    {
        updateDataItems();
        this.RegisterToAutoMapper();
        if (numberOfDataSourceInScene() > 1)
        {
            this.LogWarningForDuplicateDataSources("Emitter Data Source");
        }
    }

    void Update()
    {
        updateDataItems();
    }
}

}
