﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Ultrahaptics
{

public class ArrayDataSource : MonoBehaviour, IDataSource
{
    private Dictionary<string, UnityEngine.Vector3> _data = new Dictionary<string, UnityEngine.Vector3>();

    public string[] GetAvailableDataItemsForType<T>()
    {
        if(typeof(T) == typeof(UnityEngine.Vector3))
        {
            return _data.Keys.ToArray();
        }
        return null;
    }

    public T GetDataItemByName<T>(string name)
    {
        if(!_data.ContainsKey(name))
        {
            throw new NullReferenceException();
        }
        return (T)(object)_data[name]; // Force conversion from Vector3 to T (which must be Vector3)
    }

    UnityEngine.Vector3 getPosition()
    {
        return this.gameObject.transform.position;
    }

    UnityEngine.Vector3 getDirection()
    {
        var rotation = this.gameObject.transform.rotation;
        var direction = UnityEngine.Vector3.forward;
        return rotation * direction;
    }

    UnityEngine.Vector3 getNormal()
    {
        var rotation = this.gameObject.transform.rotation;
        var normal = UnityEngine.Vector3.up;
        return rotation * normal;
    }

    int numberOfArrayDataSourceInScene()
    {
        return GameObject.FindObjectsOfType<ArrayDataSource>().Length;
    }

    void Start ()
    {
        _data.Add("arrayPosition", getPosition());
        _data.Add("arrayDirection", getDirection());
        _data.Add("arrayNormal", getNormal());
        this.RegisterToAutoMapper();
        if (numberOfArrayDataSourceInScene() > 1)
        {
            this.LogWarningForDuplicateDataSources("Array Data Source");
        }
    }

    void Update ()
    {
        _data["arrayPosition"] =  getPosition();
        _data["arrayDirection"] =  getDirection();
        _data["arrayNormal"] =  getNormal();
    }
}

}
