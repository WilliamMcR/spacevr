using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using UnityEngine;

namespace Ultrahaptics
{
    public class LeapDataSource : MonoBehaviour, IDataSource
    {
        private Dictionary<string, UnityEngine.Vector3> _data = new Dictionary<string, UnityEngine.Vector3>();
        private UnityEngine.Object _hand;
        private UnityEngine.Object[] _fingers;
        private Type _handModelType;
        private Type _fingerModelType;
        private FieldInfo _fingersInfo;
        private MethodInfo _getFingerTipMethod;
        private MethodInfo _getPalmPositionMethod;
        private MethodInfo _getPalmDirectionMethod;
        private MethodInfo _getPalmNormalMethod;
        private string[] _fingerPositionsNames = new string[]{
            "thumbPosition",
            "indexFingerPosition",
            "middleFingerPosition",
            "ringFingerPosition",
            "pinkyFingerPosition"
        };

        public string[] GetAvailableDataItemsForType<T>()
        {
            if(typeof(T) == typeof(UnityEngine.Vector3))
            {
                return _data.Keys.ToArray();
            }
            return null;
        }

        public T GetDataItemByName<T>(string name)
        {
            if(!_data.ContainsKey(name))
            {
                throw new NullReferenceException();
            }
            return (T)(object)_data[name]; // Force conversion from Vector3 to T (which must be Vector3)
        }

        void Start ()
        {
            getReflectionData();
            this.RegisterToAutoMapper();
        }

        private bool handIsPresent()
        {
            return _hand != null;
        }

        private void findHand()
        {
            if(_handModelType == null)
            {
                return;
            }
            _hand = FindObjectOfType(_handModelType);
        }

        void Update ()
        {
            if (!handIsPresent())
            {
                defaultPalmPositionAndOrientation();
                defaultFingersPosition();
                findHand();
            }
            if (handIsPresent())
            {
                assignPalmPositionAndOrientation();
                assignFingersPositions();
            }
        }

        private Type getTypeByReflection(string typeName)
        {
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => !(a.ManifestModule is System.Reflection.Emit.ModuleBuilder))
                .SelectMany(a => a.GetExportedTypes())
                .Where(t => t.Name == typeName)
                .ToArray();

            return types.Length > 1
                ? types.FirstOrDefault(t => !t.FullName.Contains("Ultrahaptics"))
                : types.FirstOrDefault();
        }

        private void getReflectionData()
        {
            _handModelType = getTypeByReflection("HandModel");
            _fingerModelType = getTypeByReflection("FingerModel");
            _getPalmPositionMethod = _handModelType.GetMethod("GetPalmPosition");
            _getPalmDirectionMethod = _handModelType.GetMethod("GetPalmDirection");
            _getPalmNormalMethod = _handModelType.GetMethod("GetPalmNormal");
            _getFingerTipMethod = _fingerModelType.GetMethod("GetTipPosition");
            _fingersInfo = _handModelType.GetField("fingers");

        }

        private void defaultFingersPosition()
        {
            for(int index=0; index < _fingerPositionsNames.Count(); index++ )
            {
                string fingerPositionName = _fingerPositionsNames[index];
                _data[fingerPositionName] = UnityEngine.Vector3.zero;
            }
        }

        private void assignFingersPositions()
        {
            _fingers = (UnityEngine.Object[])_fingersInfo.GetValue(_hand);

            for(int index=0; index < _fingerPositionsNames.Count(); index++ )
            {
                string fingerPositionName = _fingerPositionsNames[index];
                _data[fingerPositionName] = (UnityEngine.Vector3)_getFingerTipMethod.Invoke(_fingers[index], null);
            }
        }

        private void defaultPalmPositionAndOrientation()
        {

            _data["palmPosition"] = UnityEngine.Vector3.zero;
            _data["palmDirection"] = UnityEngine.Vector3.zero;
            _data["palmNormal"] = UnityEngine.Vector3.zero;
        }

        private void assignPalmPositionAndOrientation()
        {
            _data["palmPosition"] = (UnityEngine.Vector3)_getPalmPositionMethod.Invoke(_hand, null);
            _data["palmDirection"] = (UnityEngine.Vector3)_getPalmDirectionMethod.Invoke(_hand, null);
            _data["palmNormal"] = (UnityEngine.Vector3)_getPalmNormalMethod.Invoke(_hand, null);
        }
    }
}
