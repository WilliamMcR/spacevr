using UnityEditor;
using UnityEngine;

namespace Ultrahaptics {
public class HardwareInfoEditorWindow : EditorWindow
{
    private const string NO_DEVICE_MODEL_IDENTIFIER = "No Ultrahaptics Device detected";
    private const string NO_DEVICE_SERIAL_NUMBER = "";
    private const string NO_DEVICE_FIRMWARE_VERSION = "";

    private string modelIdentifier_ = NO_DEVICE_MODEL_IDENTIFIER;
    public string ModelIdentifier
    {
        get
        {
            return modelIdentifier_;
        }
    }

    private string serialNumber_ = NO_DEVICE_SERIAL_NUMBER;
    public string SerialNumber
    {
        get
        {
            return serialNumber_;
        }
    }

    private string firmwareVersion_ = NO_DEVICE_FIRMWARE_VERSION;
    public string FirmwareVersion
    {
        get
        {
            return firmwareVersion_;
        }
    }

    private double timeOfLastUpdate_ = 0;
    private const double pollIntervalInSeconds_ = 1;

    private bool isHardwareConnected()
    {
        return SensationCore.Instance.IsEmitterConnected();
    }

    private void updateLastSeenHardwareToBeDisconnectedState()
    {
        modelIdentifier_ = NO_DEVICE_MODEL_IDENTIFIER;
        serialNumber_ = NO_DEVICE_SERIAL_NUMBER;
        firmwareVersion_ = NO_DEVICE_FIRMWARE_VERSION;
    }

    private void updateLastSeenHardwareToBeCurrentHardware()
    {
        modelIdentifier_ = "Not implemented"; //SensationSource.SensationCore.EmitterModelIdentifier();
        serialNumber_ = "Not implemented"; //SensationSource.SensationCore.EmitterSerialNumber();
        firmwareVersion_ = "Not implemented"; //SensationSource.SensationCore.EmitterFirmwareVersion();
    }

    public void updateLastSeenInformationFromCurrentDevice()
    {
        if (isHardwareConnected())
        {
            updateLastSeenHardwareToBeCurrentHardware();
        }
        else
        {
            try
            {
                SensationCore.Instance.AcquireEmitter();
                if (isHardwareConnected())
                {
                    updateLastSeenHardwareToBeCurrentHardware();
                }
                else
                {
                    updateLastSeenHardwareToBeDisconnectedState();
                }
                SensationCore.Instance.ReleaseEmitter();
            }
            catch
            {
                updateLastSeenHardwareToBeDisconnectedState();
                // No Error to report while polling
            }
        }
    }

// TODO: re-enable this menu window when upgraded Unity to 2018
    // [MenuItem("Ultrahaptics/Hardware Info")]
    static void Init()
    {
        HardwareInfoEditorWindow window = (HardwareInfoEditorWindow)EditorWindow.GetWindow(typeof(HardwareInfoEditorWindow));
        window.Show();
    }

    // Couroutines do not work in EditMode, so the alternative to having Hardware polling in a coroutine is to create a C# thread.
    // Which is too much for what we really want. OnGui is called more often than we would want to poll so can get approximate update
    // frequency by putting a counter in here.
    // *** With the caveat that the user must interact with the window to trigger this event ***
    // An example of this can be found here : https://docs.unity3d.com/ScriptReference/EditorApplication-timeSinceStartup.html
    void OnGUI()
    {
        if (EditorApplication.timeSinceStartup > timeOfLastUpdate_ + pollIntervalInSeconds_)
        {
            updateLastSeenInformationFromCurrentDevice();
            timeOfLastUpdate_ = EditorApplication.timeSinceStartup;
        }

        GUILayout.Label("Ultrahaptics Device Info", EditorStyles.boldLabel);

        modelIdentifier_ = EditorGUILayout.TextField("Device:", modelIdentifier_);
        serialNumber_ = EditorGUILayout.TextField("Serial:", serialNumber_);
        firmwareVersion_ = EditorGUILayout.TextField("Firmware:", firmwareVersion_);

        Texture icon = AssetDatabase.LoadAssetAtPath<Texture> ("Assets/UltrahapticsCoreAsset/Resources/SensationSource.png");
        titleContent.text = "Hardware Info";
        titleContent.image = icon;
    }
}
}