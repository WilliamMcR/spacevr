using UnityEditor;
using UnityEngine;

namespace Ultrahaptics.Editor
{
    [CustomPropertyDrawer(typeof(HapticTriggerRegion.EditorHelpBoxAttribute))]
    public class HapticTriggerRegionEditor : PropertyDrawer
    {
        public HapticTriggerRegionEditor()
        {
            _editor = EditorImpl.Instance;
        }
        public HapticTriggerRegionEditor(IEditor editor)
        {
            _editor = editor;
        }

        IEditor _editor;
        const float _margin = 2f;
        float _size = EditorGUIUtility.singleLineHeight * 2;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return (_size * 2) + _margin;
        }

        public override void OnGUI(Rect p, SerializedProperty property, GUIContent label)
        {
            var target = (Component)property.serializedObject.targetObject;

            var r1 = new Rect(p.x, p.y, p.width, _size);
            var r2 = new Rect(p.x, r1.yMax + _margin, p.width, _size);

            var hasCollider = target.GetComponent<Collider>();
            if (hasCollider)
            {
                _editor.HelpBox(r1, "Found collider: " + hasCollider, MessageType.Info);
            } else {
                _editor.HelpBox(r1, "No collider", MessageType.Error);
            }

            var mapperExists = Object.FindObjectOfType<AutoMapper>();
            if (mapperExists)
            {
                _editor.HelpBox(r2, "Mapper exists", MessageType.Info);
            } else {
                _editor.HelpBox(r2, "No AutoMapper found", MessageType.Warning);
            }
        }
    }
}
