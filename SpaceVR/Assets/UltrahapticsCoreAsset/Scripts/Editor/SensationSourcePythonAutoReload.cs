using UnityEditor;
using UnityEngine;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace Ultrahaptics.Editor
{
    public class SensationSourcePythonAutoReload : AssetPostprocessor
    {
        
        public static void OnPostprocessAllAssets(string[] importedAssets,
            string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            if (importedAssets
                .Concat(deletedAssets)
                .Concat(movedAssets)
                .Concat(movedFromAssetPaths)
                .Any(f => Path.GetExtension(f).ToLower() == ".py")
            ) {
                // We need to completely reset SCL to clear the python module cache
                if(SensationCore.Instance != null)
                {
                    SensationCore.Instance.Dispose();
                    SensationCore.ScriptReload();
                    foreach (var cmp in Resources.FindObjectsOfTypeAll<SensationSource>())
                    {
                        cmp.RecreateCurrentBlock();
                    }
                }
            }
        }
    }
}
