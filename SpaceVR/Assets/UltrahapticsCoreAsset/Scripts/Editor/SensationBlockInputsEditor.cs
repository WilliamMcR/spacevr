using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;

namespace Ultrahaptics.Editor
{
    
    [CustomPropertyDrawer(typeof(SensationBlockInputs))]
    public class SensationBlockInputsEditor : PropertyDrawer
    {
        private int height = 20;
        private float totalHeight = 0;
        
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            SerializedProperty Inputs = property.FindPropertyRelative("Inputs").Copy();
            int i;
            for (i = 0; i < Inputs.arraySize; i++)
            {
                SerializedProperty input = Inputs.GetArrayElementAtIndex(i);
                Rect pos = position;
                pos.y += i * height;
                EditorGUI.BeginChangeCheck();
                string name = input.FindPropertyRelative("name").stringValue;
                switch (name)
                {
                    case "t":
                        EditorGUI.TextArea(new Rect(pos.x,pos.y,pos.width,height - 2), "This sensation changes with time", GUI.skin.GetStyle("HelpBox"));
                        EditorGUI.EndChangeCheck();
                        break;
                    default:
                        UnityEngine.Vector3 value = EditorGUI.Vector3Field(pos , input.FindPropertyRelative("name").stringValue, input.FindPropertyRelative("value").vector3Value);
                        if (EditorGUI.EndChangeCheck())
                            input.FindPropertyRelative("value").vector3Value = value;
                        break;
                }
            }
            totalHeight = i *height;
            EditorUtility.SetDirty(property.serializedObject.targetObject);
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return totalHeight;
        }
    }
}
