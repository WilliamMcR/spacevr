﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ultrahaptics.Examples
{
    public class SensationSourcePlaybackDurationExample : MonoBehaviour
    {
        public SensationSource sensationSource;
        private Slider slider;
        public Text durationText;
        // Use this for initialization
        void Start()
        {
            slider = GetComponentInChildren<Slider>();
        }

        public void OnClick()
        {
            sensationSource.RunForDuration(slider.value);
        }

        public void Update()
        {
            durationText.text =
                "Sensation Sources can also be run for a duration via the following C# method\n\n<b>SensationSource.RunForDuration(float duration)</b>\n\n" +
                "Click the <b>Run for Duration</b> button to feel a Circle for " + (Mathf.Round(slider.value * 10f) * 0.1f) + " seconds";
        }
    }
}