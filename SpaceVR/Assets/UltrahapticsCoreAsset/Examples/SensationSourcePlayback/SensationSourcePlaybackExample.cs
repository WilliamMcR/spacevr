﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ultrahaptics.Examples
{
    public class SensationSourcePlaybackExample : MonoBehaviour {

        private Text displayText;
        private SensationEmitter sensationEmitter;

        private void Start()
        {
            displayText = GetComponent<Text>();
            sensationEmitter = GameObject.FindObjectOfType<SensationEmitter>();
        }

        // Update is called once per frame
        void Update () {
            if(sensationEmitter != null)
                displayText.text = "<b>Current Sensation: " + sensationEmitter.CurrentSensation.sensationBlock + "</b>";
	    }
    }
}
