# Ultrahaptics Core Asset - Block Manifest

This file describes the Sensation Blocks available to the Ultrahaptics Core Asset for Unity.

A _'Sensation-producing'_ Block (one that outputs control points) can by added to Unity be entering its name into the *Sensation Block* field of a *Sensation Source* component.

You can use the Blocks described below to build your own Block graphs, by creating an instance of that Block (using `createInstance`), and connecting Block inputs and outputs (using `connect`)

For more information on creating Block Graphs via Python, refer to the **pysensationcore** Python module, included in StreamingAssets/Python.



## Types

### transform

Used to store or manipulate position, rotation, scale and other types of affine transform.

### path

A channel-based type representing a finite-length path in 3-dimensional space.

Evaluation of the `point` forward channel with a value between 0.0 and 1.0 on
the `u` backward channel will yield a point on the path.  Sweeping the `u`
value through its range will visit every point on the path.

The result of evaluating `point` for `u` values outside of the valid range
is undefined.

Paths can be input to the **RenderPath** Block to produce control point output.

### scalar

A 3-tuple where the first element carries a real-valued scalar and the
remaining two values are ignored

### time

Synonym for `scalar`.  Used to denote time values in seconds.

### uhsclVector3_t

A 3-tuple of real-valued numbers.  Used to represent position or direction
in 3-dimensional space.

#### Channels

* `u` (backward, scalar) Supply a value in the range 0.0 <= `u` <= 1.0
* `point` (forward, uhsclVector3_t) point on the path corresponding to
          the given `u` value

## Blocks

### **CirclePath**

* Sensation-producing: **NO**

Outputs a circular path, with given radius, in the z=0 plane

#### Inputs:

* `radius` (scalar): The radius of the circle.

#### Output:

* `out` (path): A circle of the given `radius`

### **CircleSensation**

* Sensation-producing: **YES**

Outputs a circular path, with given radius, at the given offset

#### Inputs:

* `radius` (uhsclVector3_t): the radius of the circle
* `offset` (uhsclVector3_t): the offset of the circle in virtual space

The following inputs are recommended to be automapped using `VirtualSpaceDataSource` and `EmitterDataSource`
* `sensationXInVirtualSpace` (uhsclVector3_t): direction of the sensation space x axis in virtual space
* `sensationYInVirtualSpace` (uhsclVector3_t): direction of the sensation space y axis in virtual space
* `sensationZInVirtualSpace` (uhsclVector3_t): direction of the sensation space z axis in virtual space
* `sensationOriginInVirtualSpace` (uhsclVector3_t): origin of the sensation space axis in virtual space

* `virtualEmitterXInVirtualSpace` (uhsclVector3_t): direction of the virtual emitter x axis in virtual space
* `virtualEmitterYInVirtualSpace` (uhsclVector3_t): direction of the virtual emitter y axis in virtual space
* `virtualEmitterZInVirtualSpace` (uhsclVector3_t): direction of the virtual emitter z axis in virtual space
* `virtualEmitterOriginInVirtualSpace` (uhsclVector3_t): origin of the virtual emitter axis in virtual space

* `virtualXInEmitterSpace` (uhsclVector3_t): direction of the virtual x axis in emitter space
* `virtualYInEmitterSpace` (uhsclVector3_t): direction of the virtual y axis in emitter space
* `virtualZInEmitterSpace` (uhsclVector3_t): direction of the virtual z axis in emitter space
* `virtualOriginInEmitterSpace` (uhsclVector3_t): origin of the virtual axis in emitter space

#### Output:

* `out` (uhsclVector3_t): A circle of the given `radius` and `offsetInVirtualSpace`

### **Comparator**

Compares two values (a and b) and returns a value depending on the result of comparison cases:
`a` greater than `b`, `a` equal to `b`, or `a` less than `b`.

* Sensation-producing: **NO**

#### Inputs:

* `a` (scalar): First value to be compared
* `b` (scalar): Second value to be compared
* `returnValueIfAGreaterThanB` (uhsclVector3_t): Returned value when `a` is greater than `b`
* `returnValueIfAEqualsB` (uhsclVector3_t): Returned value when `a`, is equal to `b`
* `returnValueIfALessThanB` (uhsclVector3_t): Returned value when `a` is less than `b`

#### Output:
* `out` (scalar): Outputs value depending comparisson

### **ComposeInverseTransform**

* Sensation-producing: **NO**

Calculate the inverse of a transform using the component vectors.

#### Inputs:

* `x` (uhsclVector3_t): the value of the x vector (first column)
* `y` (uhsclVector3_t): the value of the y vector (second column)
* `z` (uhsclVector3_t): the value of the z vector (third column)
* `o` (uhsclVector3_t): the offset of the transform

#### Outputs:

* `out` (transform): the inverse of the composed transform


### **ComposeTransform**

* Sensation-producing: **NO**

Compose a transform using the component vectors.

#### Inputs:

* `x` (uhsclVector3_t): the value of the x vector (first column)
* `y` (uhsclVector3_t): the value of the y vector (second column)
* `z` (uhsclVector3_t): the value of the z vector (third column)
* `o` (uhsclVector3_t): the offset of the transform

#### Outputs:

* `out` (transform): the composed transform

### **IntensityWave**

* Sensation-producing: **NO**

Calculates the value of a cosine wave given a modulationFrequency at a point in time. It varies between 0 and 1.

#### Inputs:

* `t` (scalar): point in time
* `modulationFrequency` (scalar): frequency of the cosine wave (default = 143)

#### Outputs:

* `out` (scalar): value of the cosine wave at point t

### **CrossProduct**

* Sensation-producing: **NO**

Calculate the cross product of two 3-vectors.

#### Inputs:

* `lhs` (uhsclVector3_t): left-hand-side operand to the cross product
* `rhs` (uhsclVector3_t): right-hand-side operand to the cross product

#### Outputs:

* `out` (uhsclVector3_t): the cross-product lhs x rhs

### **DialSensation**

* Sensation-producing: **YES**

Dial Sensation produces a circular sensation moving along a circular path

#### Inputs:

* `innerRadius` (scalar): radius of the sensation translated along the circular path (default =  0.025)
* `outerRadius` (scalar): radius of the circular path (default = 0.05)
* `rate` (scalar): determines how fast the circular sensation moves along the circular path.
                   Negative values change the direction of the path.
* `offset` (uhsclVector3_t): the x,y,z origin of the circular path (default = (0.0, 0.0, 0.0) )

#### Outputs:

* `out` (uhsclVector3_t): Output port, producing control points for the Dial Sensation

### **ExpandingCircleSensation**

* Sensation-producing: **YES**

Draws a Circle Sensation which expands from an initial radius to a final radius, over a given duration specified in seconds.

#### Inputs:

* `duration` (scalar): The duration in seconds over which the circle expands from start to end radius (default: 1.0)
* `startRadius` (scalar): the starting radius of the circle, specified in meters (default 0.005)
* `endRadius` (scalar): the final radius of the circle, specified in meters (default 0.1)
* `offset` (uhsclVector3_t): the x,y,z offset of the expanding circle (default = (0.0, 0.0, 0.0) )


The following inputs are recommended to be automapped using `VirtualSpaceDataSource` and `EmitterDataSource`
* `sensationXInVirtualSpace` (uhsclVector3_t): direction of the sensation space x axis in virtual space
* `sensationYInVirtualSpace` (uhsclVector3_t): direction of the sensation space y axis in virtual space
* `sensationZInVirtualSpace` (uhsclVector3_t): direction of the sensation space z axis in virtual space
* `sensationOriginInVirtualSpace` (uhsclVector3_t): origin of the sensation space axis in virtual space

* `virtualEmitterXInVirtualSpace` (uhsclVector3_t): direction of the virtual emitter x axis in virtual space
* `virtualEmitterYInVirtualSpace` (uhsclVector3_t): direction of the virtual emitter y axis in virtual space
* `virtualEmitterZInVirtualSpace` (uhsclVector3_t): direction of the virtual emitter z axis in virtual space
* `virtualEmitterOriginInVirtualSpace` (uhsclVector3_t): origin of the virtual emitter axis in virtual space

* `virtualXInEmitterSpace` (uhsclVector3_t): direction of the virtual x axis in emitter space
* `virtualYInEmitterSpace` (uhsclVector3_t): direction of the virtual y axis in emitter space
* `virtualZInEmitterSpace` (uhsclVector3_t): direction of the virtual z axis in emitter space
* `virtualOriginInEmitterSpace` (uhsclVector3_t): origin of the virtual axis in emitter space

#### Output:

* `out`: Output port, producing control points for the Expanding Circle Sensation

### **Inverse**

Returns the inverse of a scalar value. If value is 0 the block ouptut will be ( 0 , 0 , 0 ) .
* Sensation-producing: **NO**

#### Inputs:

* `value` (scalar): Value to calculate inverse

#### Output:
* `out` (scalar): inverse of value  (1 / value)

### **Lerp**

Linearly interpolates between x0 and x1.
For a value x in the interval ( y0 , y1 ), the corresponding value is interpolated in the range ( x0 , x1 ) .

* Sensation-producing: **NO**

#### Inputs:

* `x` (scalar): Value to vary in the interpolation range ( y0 , y1 )
* `x0` (scalar): The initial interpolated value
* `x1` (scalar): The end interpolated value
* `y0` (scalar): The initial value of the interpolation range (defaults 0.0)
* `y1` (scalar): The end value of the interpolation range

#### Output:
* `out` (scalar): Outputs the interpolated value for x

### **LinePath**

Outputs a linear path between two endpoints.

#### Inputs:

* `endpointA` (uhsclVector3_t): The start point of the line path.
* `endpointB` (uhsclVector3_t): The end point of the line path.

#### Output:
* `out` (path): A line between the given endpoints.

* Sensation-producing: **NO**

### **LineSensation**

* Sensation-producing: **YES**

Outputs a line, with given endpoints, at the given offset

#### Inputs:

* `endpointA` (uhsclVector3_t): the start point of the line
* `endpointB` (uhsclVector3_t): the end point of the line
* `offset` (uhsclVector3_t): the offset of the line in virtual space

The following inputs are recommended to be automapped using `VirtualSpaceDataSource` and `EmitterDataSource`
* `sensationXInVirtualSpace` (uhsclVector3_t): direction of the sensation space x axis in virtual space
* `sensationYInVirtualSpace` (uhsclVector3_t): direction of the sensation space y axis in virtual space
* `sensationZInVirtualSpace` (uhsclVector3_t): direction of the sensation space z axis in virtual space
* `sensationOriginInVirtualSpace` (uhsclVector3_t): origin of the sensation space axis in virtual space

* `virtualEmitterXInVirtualSpace` (uhsclVector3_t): direction of the virtual emitter x axis in virtual space
* `virtualEmitterYInVirtualSpace` (uhsclVector3_t): direction of the virtual emitter y axis in virtual space
* `virtualEmitterZInVirtualSpace` (uhsclVector3_t): direction of the virtual emitter z axis in virtual space
* `virtualEmitterOriginInVirtualSpace` (uhsclVector3_t): origin of the virtual emitter axis in virtual space

* `virtualXInEmitterSpace` (uhsclVector3_t): direction of the virtual x axis in emitter space
* `virtualYInEmitterSpace` (uhsclVector3_t): direction of the virtual y axis in emitter space
* `virtualZInEmitterSpace` (uhsclVector3_t): direction of the virtual z axis in emitter space
* `virtualOriginInEmitterSpace` (uhsclVector3_t): origin of the virtual axis in emitter space

#### Output:

* `out` (uhsclVector3_t): A line of the given endpoints and `offsetInVirtualSpace`


### **Lissajous**

Lissajous Sensation which produces a Lissajous curve path defined using Size X, Size Y, Parameter A, Parameter Y, drawFrequency.

The Lissajous parametric equations in X,Y axes are defined as:

x = SizeX*cos(A*2*pi*f*t)
y = SizeY*sin(B*2*pi*f*t)

Visually, the ratio of A/B parameters determines the number of "lobes" of the Lissajous figure. For example, a ratio of 3/1 or 1/3 produces a figure with three major lobes.

**Inputs:**
* `sizeX`: the amplitude of the Lissajous along the X-axis in meters (default = 0.01)
* `sizeY`: the amplitude of the Lissajous along the Y-axis in meters (default = 0.01)
* `paramA`: the A parameter of the Lissajous curve (default = 3)
* `paramB`: the B parameter of the Lissajous curve (default = 2)
* `offset`: the x,y,z position of the Lissajous
* `drawFrequency`: Number of times per second that the Lissajous is drawn (default = 40Hz)

### **RenderPath**

Evaluates a path (e.g. **LinePath**) to produce control point positions.
Note: The RenderPath Block does not produce control point output, unless
it receives a valid path-producing input, to its **path** input.

* Sensation-producing: **NO**

#### Inputs:

* `path` (path): The path to render
* `period` (scalar): The period (in seconds) for RenderPath to render one complete path.
* `t` (time): The time since the start of a Block's evaluation (in seconds).

#### Output:
* `out` (uhsclVector3_t): Outputs control point positions (if a valid
                          path has been specified)


### **Point**

Outputs an intensity-modulated control point offset 0.2 in Z. This can
be used as a trivial Sensation-producing Block for test purposes.

* Sensation-producing: **YES**

#### Inputs:

* `t` (time): The time since the start of a Block's evaluation (in seconds).

#### Output:
* `out` (uhsclVector4_t): Control point with modulated intensity offset by 0.2 in Z.


### **SensationSpaceToVirtualHandSpace**

Transforms a point from Sensation Space to Virtual Hand Space.
Use this Block if your need your Sensation designed in _Sensation Space_ to map to the hand in _Virtual Hand Space_ (provided by the tracking device)
Note: This Block outputs a point in _Virtual Space_ and is typically used in conjunction with a Block that transforms from _Virtual_ to _Emitter Space_.

* Sensation-producing: **NO**

#### Inputs:

* `point` (uhsclVector3_t): Point in, defined in _Sensation Space_
* `palmPosition` (uhsclVector3_t): The position of the hand in _Virtual Space_
* `palmDirection` (uhsclVector3_t): The direction of the hand in _Virtual Space_
* `palmNormal` (uhsclVector3_t): The normal of the hand in _Virtual Space_

#### Output:
* `out` (uhsclVector3_t): Control point out, defined in _Virtual Space_

### **VirtualSpaceToEmitterSpace**

Converts coordinates of a point from _Virtual Space_ to _Emitter Space_,
given the position and orientation of an array in _Virtual Space_ and the
coordinates of a point in _Virtual Space_.

* Sensation-producing: **NO**

#### Inputs

* `arrayPosition` (uhsclVector3_t): position of the centre of the array
                                    in _Virtual Space_
* `arrayDirection` (uhslcVector3_t): unit vector in _Virtual Space_ pointing
                                     from the centre of the array to the centre
                                     of the top edge
* `arrayNormal` (uhsclVector3_t): unit vector in _Virtual Space_ pointing
                                  out of the top of the array normal to
                                  its surface
* `point` (uhsclVector3_t): point in _Virtual Space_ to be transformed
                                 into _Emitter Space_

#### Output:

* `out`: (uhsclVector3_t): coordinates of the given `point` in _Emitter Space_

### **TransformPath**

Apply a transform to a path to generate a new path

#### Inputs

* `path` (path): Path to transform
* `transform` (uhsclMatrix4x4_t): transform to apply to `path`

#### Output

* `out` (path): A new path resulting from applying `transform` to `path`

### **TranslateAlongPath**

Animates the offset of a path (the `objectPath`) along another path
(the `animationPath`) over a period of `duration` seconds

* Sensation-producing: **NO**

#### Inputs

* `t` (time): Time within animation duration
* `duration` (uhsclVector3_t): Duration of the animation along the
                               animation path in seconds
* `direction` (scalar): if equals ( 1 , 0 , 0 ) the direction of the sensation is inverted, if ( 0 , 0 , 0 ) the direction doesn't change
* `animationPath` (path): Path that the translated origin of the object
                          path will follow
* `objectPath` (path): Path defining the shape to be animated along the
                       animation path

