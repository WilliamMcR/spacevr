﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateAsteroids : MonoBehaviour {

    public GameObject[] asteroidPrefab;
    public float fieldRadius;
    public float movementSpeed;
    public float rotationSpeed;

    // Use this for initialization
    void Start() {
        for (int i = 0; i < 50; i++)
        {
            GameObject newAsteroid = (GameObject)Instantiate(asteroidPrefab[Random.Range(0,1)], Random.insideUnitCircle * fieldRadius, Random.rotation);
            float size = Random.Range(0.0001f, 0.001f);
            newAsteroid.transform.localScale = Vector3.one * size;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
