﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Ultrahaptics;

public class Unity_TPSFocus_Backup : MonoBehaviour {

    TimePointStreamingEmitter _emitter;

    uint _current, _timepoint_count;

    Ultrahaptics.Vector3[] _positions;
    float[] _intensities;

    bool emitting;
    public bool STM;

	// Use this for initialization
	void Start () {
        _emitter = new TimePointStreamingEmitter();

        uint sample_rate = _emitter.setMaximumControlPointCount(1);
        float desired_frequency = 200.0f;

        _timepoint_count = (uint)(sample_rate / desired_frequency);

        _positions = new Ultrahaptics.Vector3[_timepoint_count];
        _intensities = new float[_timepoint_count];

        STM = false;


        for (int i = 0; i < _timepoint_count; i++)
        {
            float intensity = 0.5f * (1.0f - (float)Math.Cos(2.0f * Math.PI * i / _timepoint_count));
            // Set a constant position of 20cm above the array
            if(STM)
                _positions[i] = new Ultrahaptics.Vector3((float)i * 0.1f, (float)i * 0.1f, (float)i * 0.1f);
            else
                _positions[i] = new Ultrahaptics.Vector3(0.0f, 0.0f, 0.2f);
            _intensities[i] = (intensity);
        }
        
        // Set our callback to be called each time the device is ready for new points
        _emitter.setEmissionCallback(callback, null);
        // Instruct the device to call our callback and begin emitting
        _emitter.start();
        emitting = true;
    }

    void callback(TimePointStreamingEmitter emitter, OutputInterval interval, TimePoint deadline, object user_obj)
    {
        // For each time point in this interval...
        foreach (var tpoint in interval)
        {
            // For each control point available at this time point...
            for (int i = 0; i < tpoint.count(); ++i)
            {
                // Set the relevant data for this control point
                var point = tpoint.persistentControlPoint(i);
                point.setPosition(_positions[_current]);
                point.setIntensity(_intensities[_current]);
                point.enable();
            }
            // Increment the counter so that we get the next "step" next time
            _current = (_current + 1) % _timepoint_count;
        }
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (emitting)
                Stop();
            else
                Start();

        }
    }

    void Stop()
    {
        // Stop the device
        _emitter.stop();

        // Dispose/destroy the emitter
        _emitter.Dispose();
        _emitter = null;
        emitting = false;
    }
}
