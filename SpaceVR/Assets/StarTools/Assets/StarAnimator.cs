﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * The CoronaRotator is a simple billboarding script- The corona effect is actually a thin disc, and should always be facing the main camera.
 */
[ExecuteInEditMode]
public class StarAnimator : MonoBehaviour {
    public Camera starLockon;
    public bool varySize;
    float time;
    void Update() {
        time = Time.time;
        if (starLockon == null) {
            transform.LookAt(Camera.main.transform);
        } else {
            transform.LookAt(starLockon.transform);
        }
        if (varySize)
        {
            transform.localScale = transform.localScale * (float)Math.Sin(time * Math.PI);
        }
    }
}