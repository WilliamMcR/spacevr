﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{

    public float halfSize;
    public float speed;
    float scale, speedConstant;
    public bool rotating;

    // Use this for initialization
    void Start()
    {
        halfSize = 0.07f;
        scale = transform.localScale.z * 100f;
        speedConstant = 180.0f;
    }

    // Update is called once per frame
    void Update()
    {
        // Vector3 point = gameObject.transform.position;
        Vector3 point = transform.position + (transform.forward * halfSize * scale);
        transform.RotateAround(point, transform.up, -(speed * Time.deltaTime * speedConstant));
    }
}
